<?PHP
		session_start();
		include("config.php");
		include("header.php");
		unset($_POST['compare']);
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>NGO Details Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">Compare NGOs
            </h3>
            <hr>
			 <form action="comparengos.php" method="post">
			<div id="custom-search-input">
                <div class="input-group col-md-12">
				
					<?php 
				$conn = mysqli_connect($host,$username,$password,$db_name);
				$page=null;
				
				$q1="Select name_ngo from ngo_profile"; 
								
				$result = $conn->query($q1);

				if ($result->num_rows > 0) {
					// output data of each row
					?><select name="s1"><?php
					while($row = $result->fetch_assoc()) {
				
				?>
				
				  <option value="<?php echo $row['name_ngo']; ?>"><?php echo $row['name_ngo']; ?></option>
				
				<?php
					}
				}
				?></select><?php
				$q1="Select name_ngo from ngo_profile"; 
								
				$result = $conn->query($q1);

				if ($result->num_rows > 0) {
					// output data of each row
					?><select name="s2" style="margin-left:20%"><?php
					while($row = $result->fetch_assoc()) {
				
				?>
				
				  <option value="<?php echo $row['name_ngo']; ?>"><?php echo $row['name_ngo']; ?></option>
				
				<?php
					}
				}
				?></select>
                    <!--<input type="text" class="form-control input-lg" placeholder="Enter NGO 1 Name" name="searchele1"/><br/>
                    <input type="text" class="form-control input-lg" placeholder="Enter NGO 2 Name" name="searchele2" />-->
                    <span class="input-group-btn" style="margin-left:20%">
                        <button class="btn btn-info btn-lg" type="submit" name="compare" id="compare">
                            Compare
                        </button>
                    </span>
                </div>
            </div>
			</form>
		  </div>
		</div>
	</div>
</header>


	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>