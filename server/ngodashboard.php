<?php 

	session_start();
	include("config.php");
	include("header.php");
	
	if(isset($_SESSION["user_id"]))
	{
		if($_SESSION["user_type"] != "ngo"){
			unset($_SESSION);
			header("loginhome.php");
		}
		else
		{
			$userid = $_SESSION["user_id"];
			
			// Connect to server and select databse.
			$conn=mysqli_connect($host,$username,$password,$db_name);
			if($conn->connect_error){
				die("Connection Error: ". $conn->connect_error);
			}
		}
	}
	
	if(isset($_GET["action"]))
	{
		if($_GET["action"] == "approve")
		{
			$user_id = $_GET["uid"];
			$ngo_id = $_GET["nid"];
			
				$sql = "UPDATE ngo_volunteer SET status = 'ACTIVE' WHERE user_id = '$user_id' AND ngo_id = '$ngo_id'";
				if($conn->query($sql)==true)
				{
					echo '<script>window.location="ngodashboard.php#list"</script>';
				}
				else
				{
					echo '<script>alert("Error")</script>';
					echo '<script>window.location="ngodashboard.php#list"</script>';
				}
				
		}
		if($_GET["action"] == "reject")
		{
			$user_id = $_GET["uid"];
			$ngo_id = $_GET["nid"];
			
			$sql = "UPDATE ngo_volunteer SET status = 'INACTIVE' WHERE user_id = '$user_id' AND ngo_id = '$ngo_id'";
				if($conn->query($sql)==true)
				{
					echo '<script>window.location="ngodashboard.php#list"</script>';
				}
				else
				{
					echo '<script>alert("Error")</script>';
					echo '<script>window.location="ngodashboard.php#list"</script>';
				}
				
		}
	}
	
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>NGO</title>
	<link href="jquery.paginate.css" rel="stylesheet" type="text/css">

  </head>
  
  <body id="page-top">

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>NGO FINDER</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Find a deserving NGO and make a difference. #DONATE</p>
            <!--<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>-->
			<form action="ngosearch.php" method="post">
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search NGO" name="searchele"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit" name="search">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
			</form>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="profile">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">Profile</h2>
            <hr class="light my-4">
			<div id="tab">
			<table class="table" id="t4">
			
					<tbody>
					
					<?php 
						
							$sql = "SELECT * FROM ngo_profile WHERE user_profile_id = '$userid'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
									<tr>
									<th>Name</th>
									<td><?php echo $row["name_ngo"]; ?></td>
									</tr>
									<tr>
									<th>Address</th>
									<td><?php echo $row["address"]; ?></td>
									</tr>
									<tr>
									<th>City</th>
									<td><?php echo $row["city"]; ?></td>
									</tr>
									<tr>
									<th>District</th>
									<td><?php echo $row["district"]; ?></td>
									</tr>
									<tr>
									<th>State</th>
									<td><?php echo $row["state"]; ?></td>
									</tr>
									<tr>
									<th>Pincode</th>
									<td><?php echo $row["pincode"]; ?></td>
									</tr>
									<tr>
									<th>Mobile Number</th>
									<td><?php echo $row["mobileno"]; ?></td>
									</tr>
									<tr>
									<th>Phone Number</th>
									<td><?php echo $row["phoneno"]; ?></td>
									</tr>
									<tr>
									<th>Email</th>
									<td><?php echo $row["email"]; ?></td>
									</tr>
									<tr>
									<th>Website</th>
									<td><?php echo $row["website"]; ?></td>
									</tr>
									<tr>
									<th>Contact Person</th>
									<td><?php echo $row["contactperson"]; ?></td>
									</tr>
									<tr>
									<th>Sector</th>
									<td><?php echo $row["sector"]; ?></td>
									</tr>
									<tr>
									<th>Aim/Objective</th>
									<td><?php echo $row["aim"]; ?></td>
									</tr>
									
						<?php
								
							}
							
						?>
									
								
									
					</tbody>
			</table>
			
				<button class="btn btn-info" role="button" onclick="$('#ngoform').show(); $('#tab').hide();">Edit Details</button>
				<a href="changepswd.php" class="btn btn-info" role="button">Change Password</a>
			</div>
			<div id="ngoform" style="display:none">
			<form id="form" class="form-horizontal" role="form" method="POST" action="profilechange.php" >
										<table class="table">
										<tbody>
										<tr>
										<td>
											<!--<label for="email" class="col-md-3 control-label">Enter NGO Name</label>-->
											
												<input type="text" class="form-control" name="ngoname" id="ngoname" placeholder="NGO Name" value="<?php echo $row["name_ngo"]?>">
											
										</td><td>
									
											<!--<label for="email" class="col-md-3 control-label">Email</label>-->
											
												<input type="text" class="form-control" name="nemail" id="nemail" placeholder="Email" value="<?php echo $row["email"]?>">
										
										</td><td>
										
											<!--<label for="firstname" class="col-md-3 control-label">Address</label>-->
										
												<input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php echo $row["address"]?>">
										
										</td></tr><tr><td>
									
											<!--<label for="city" class="col-md-3 control-label">City</label>-->
											
												<input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $row["city"]?>">
											
										</td><td>
									
											<!--<label for="district" class="col-md-3 control-label">District</label>-->
									
												<input type="text" class="form-control" name="district" id="district" placeholder="District" value="<?php echo $row["district"]?>">
										
										</td><td>
										
											<!--<label for="state" class="col-md-3 control-label">State</label>-->
											
												<input type="text" class="form-control" name="state" id="state" placeholder="State" value="<?php echo $row["state"]?>">
											
										</td></tr><tr><td>
											
									
											<!--<label for="pincode" class="col-md-3 control-label">Pin Code</label>-->
											
												<input type="number" class="form-control" name="pincode" id="pincode" placeholder="PinCode" value="<?php echo $row["pincode"]?>">
											
										</td><td>
										
											<!--<label for="mbno" class="col-md-3 control-label">Mobile Number</label>-->
											
												<input type="number" class="form-control" name="mbno" id="mbno" placeholder="Mobile Number" value="<?php echo $row["mobileno"]?>">
											
										</td><td>
									
											<!--<label for="phno" class="col-md-3 control-label">Phone Number</label>-->
									
												<input type="number" class="form-control" name="phoneno" id="phoneno" placeholder="Phone Number" value="<?php echo $row["phoneno"]?>">
											
										</td></tr><tr><td>
										
											<!--<label for="website" class="col-md-3 control-label">Website</label>-->
										
												<input type="text" class="form-control" name="website" id="website" placeholder="Website" value="<?php echo $row["website"]?>">
											
										</td><td>
										
											<!--<label for="contactp" class="col-md-3 control-label">Contact Person</label>-->
										
												<input type="text" class="form-control" name="contactp" id="contactp" placeholder="Contact Person" value="<?php echo $row["contactperson"]?>">
											
										</td><td>
										
											<!--<label for="contactp" class="col-md-3 control-label">Aim/Objective</label>-->
										
												<input type="text" class="form-control" name="aim" id="aim" placeholder="Aim" value="<?php echo $row["aim"]?>">
									
										</td></tr><tr><td>
										
											<label class="label">Sector</label></td><td colspan="2">
										
											<label class="radio-inline" style="float:left">
												<input type="checkbox" name="sector[]" id="sector" value="health">Health
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Education
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Environment
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Children
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Women
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Arts & Culture
											</label>
										
										</td></tr><tr>
										<td colspan="3">
										
										<div class="form-group">                                       
											
												<button id="btn-signup" type="submit" name="nchange" class="btn btn-info"><i class="icon-hand-right" onclick="$('#ngoform').hide(); $('#tab').show();"></i> Save Changes</button>
												
										</div> </td></tr>
										</tbody>
										</table>
									</form>
									<button class="btn btn-info" role="button" onclick="$('#ngoform').hide(); $('#tab').show();"><i class="icon-hand-right"></i> Cancel</button>
			</div>
          </div>
        </div>
      </div>
    </section>

    <section id="add">
		<div class="container">
			<div class="row">
			  <div class="col-lg-12 text-center">
				<h2 class="section-heading">Add/Edit Financial Details</h2>
				<hr class="my-4">
			  </div>
			</div>
		  </div>
		  <div class="container">
			<div class="row">
			  <div class="col-lg-12 mx-auto text-center">
				<a href="ngo_financials.csv"><input type="button" class="btn btn-default" value="Download Sample File"></a>
				<form method="POST" action="addngof.php" enctype="multipart/form-data">
	
					<div class="form-group">
						<div class="input-group input-file" name="ngofile">
							<input type="text" class="form-control" name="ngofile" id="ngofile" placeholder='Choose a file...' />			
							<span class="input-group-btn">
								<button class="btn-sq btn-default btn-lg btn-choose" type="button">Choose</button>
							</span>
						</div>
					</div>
	
					<div class="form-group">
						<button type="submit" class="btn-sq btn-primary btn-lg pull-right" name="submitf">Submit</button>
						<button type="reset" class="btn-sq btn-danger btn-lg pull-left">Reset</button>
					</div>
				</form>
				</div>
			</div>
		</div>
     
    </section>

    <section class="bg-primary" id="events">
      <div class="container-fluid p-0">
		<div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-white">Add Events</h2>
            <hr class="light my-4">
          </div>
        </div>
      </div>
	  <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
				<div id="eventbox" style="margin-left:160px">
						<form id="userform" class="form-horizontal" role="form" method="POST" action="ngosubmit.php" enctype="multipart/form-data"> 
							<div class="form-group">
								<!--<label for="email" class="col-md-3 control-label">Email</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="ename" id="ename" placeholder="Event Name">
								</div>
							</div>
											
							<div class="form-group">
								<!--<label for="firstname" class="col-md-3 control-label">First Name</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" onfocus="(this.type='date')" name="edate" id="edate" placeholder="Event Date">
								</div>
							</div>
							<div class="form-group">
								<!--<label for="middlename" class="col-md-3 control-label">Middle Name</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" onfocus="(this.type='time')" name="etime" id="etime" placeholder="Event Time">
								</div>
							</div>
							<div class="form-group">
								<!--<label for="lastname" class="col-md-3 control-label">Last Name</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="eplace" id="eplace" placeholder="Event Place">
								</div>
							</div>
							<div class="form-group">
								<!--<label for="password" class="col-md-3 control-label">Password</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="edesc" id="edesc" placeholder="Description">
								</div>
							</div><br>
							<div class="form-group">
								<label for="file" class="label col-md-3">Image :</label>
								<!--<form action="ngoform.php" method="post" enctype="multipart/form-data">-->					
								<input class="col-md-6 control-label" type="file"  id="eimage" name="eimage" required>
							</div><br>
							<div class="form-group">
								<!-- Button -->                                        
								<div class="col-md-offset-3 col-md-10">
									<button id="ubtn-signup" type="submit" name="submite" class="btn btn-info"><i class="icon-hand-right"></i> Submit</button>
									<!--<span style="margin-left:8px;">or</span>-->  
								</div>
							</div>
						</form>
				</div>
				
			</div>
		</div>
	</div>
      </div>
    </section>
	
	
	<section id="jobs">
      <div class="container-fluid p-0">
		<div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Post Jobs</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
	  <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
				<div id="userbox" style="margin-left:160px">
						<form id="userform" class="form-horizontal" role="form" method="POST" action="ngosubmit.php" enctype="multipart/form-data"> 
							<div class="form-group">
								<!--<label for="email" class="col-md-3 control-label">Email</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="jobp" id="jobp" placeholder="Job Position">
								</div>
							</div>
											
							<div class="form-group">
								<!--<label for="firstname" class="col-md-3 control-label">First Name</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="jobdesc" id="jobdesc" placeholder="Job Description">
								</div>
							</div>
							<div class="form-group">
								<!--<label for="middlename" class="col-md-3 control-label">Middle Name</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="jobdur" id="jobdur" placeholder="Duration">
								</div>
							</div>
							<div class="form-group">
								<!--<label for="lastname" class="col-md-3 control-label">Last Name</label>-->
								<div class="col-md-10">
									<input type="text" class="form-control" name="jobloc" id="jobloc" placeholder="Location">
								</div>
							</div><br>
							<div class="row">
								<label for="type" class="col-md-4 control-label">Job Type</label>
								<div class="col-md-4">
									<label class="radio-inline">
										<input type="radio" name="optradio" id="Full" value="Full Time"><span style="margin-left:5px">Full Time</span>
									</label>
									<label class="radio-inline" style="margin-left:20px">
										<input type="radio" name="optradio" id="Part" value="Part Time"><span style="margin-left:5px">Part Time</span>
									</label>
								</div>
							</div><br>
							<div class="form-group">
								<!-- Button -->                                        
								<div class="col-md-offset-3 col-md-10">
									<button id="ubtn-signup" type="submit" name="submitj" class="btn btn-info"><i class="icon-hand-right"></i> Submit</button>
									<!--<span style="margin-left:8px;">or</span>-->  
								</div>
							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
      </div>
    </section>
	
	<section class="bg-primary">
      <div class="container-fluid p-0">
		<div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-white">Event Participants</h2>
            <hr class="light my-4">
          </div>
        </div>
      </div>
	  <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
		  
		  <?php 
				$sql = "SELECT * FROM ngo_events WHERE user_profile_id = '$userid'";
				$result = mysqli_query($conn, $sql);
				
				if(mysqli_num_rows($result) > 0){
					while($row = mysqli_fetch_assoc($result)){
					$eventid = $row["event_id"];
					$eventname = $row["event_name"];
					$date = $row["event_date"];
			?>
					<h4 align="left"> <?php echo $eventname; ?> </h4><br>
					
					<table class="table" id="t2">
						<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Phone Number</th>
								</tr>
								
							</thead>
							<tbody>
					
			<?php
							$sql1 = "SELECT * FROM event_participants WHERE event_id = '$eventid'";
							$result1 = mysqli_query($conn, $sql1);
							
							if(mysqli_num_rows($result1) > 0){
								while($row = mysqli_fetch_assoc($result1)){
									
											$user_id = $row["user_id"];
											
											$sql2 = "SELECT * FROM user_profile WHERE user_profile_id = '$user_id'";
											$result2 = mysqli_query($conn, $sql2);
												
											if(mysqli_num_rows($result2) > 0){
												$row = mysqli_fetch_assoc($result2);				
												
			?>
													<tr>
													<td> <?php echo $row["first_name"].$row["middle_name"]." ".$row["last_name"]; ?> </td>
													<td> <?php echo $row["email"]; ?> </td>
													<td> <?php echo $row["phone"]; ?> </td>
													</tr>
												
			<?php
												
											}
												
								}
								
							}
									
			?>
							
							</tbody>
					</table> <br>
					
			<?php
					}
				}
			?>
			</div>
		</div>
	</div>
      </div>
    </section>
	
	<section id="list">
		<div class="container">
			<div class="row">
			  <div class="col-lg-12 text-center">
				<h2 class="section-heading">Volunteers</h2>
				<hr class="my-4">
			  </div>
			</div>
		  </div>
		  <div class="container">
			<div class="row">
			  <div class="col-lg-12 mx-auto text-center">
					<table class="table" id="t1">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Phone Number</th>
							<th>Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody>
					
					<?php 
						
							$sql1 = "SELECT * FROM ngo_volunteer WHERE ngo_id = '$userid'";
							$result1 = mysqli_query($conn, $sql1);
							
							if(mysqli_num_rows($result1) > 0){
								while($row = mysqli_fetch_assoc($result1)){
									$user_id = $row["user_id"];
									$status = $row["status"];
									
									$sql2 = "SELECT * FROM user_profile WHERE user_profile_id = '$user_id'";
									$result2 = mysqli_query($conn, $sql2);
										
									if(mysqli_num_rows($result2) > 0){
										$row = mysqli_fetch_assoc($result2);
									
										if($status == "ACTIVE")
										{
										
					?>
											<tr>
											<td> <?php echo $row["first_name"].$row["middle_name"]." ".$row["last_name"]; ?> </td>
											<td> <?php echo $row["email"]; ?> </td>
											<td> <?php echo $row["phone"]; ?> </td>
											<td> <?php echo $status ?> </td>
											<td></td>
											</tr>
										
					<?php
										}
										else if ($status == "PENDING")
										{
					?>
					
											<tr>
											<td> <?php echo $row["first_name"].$row["middle_name"].$row["last_name"]; ?> </td>
											<td> <?php echo $row["email"]; ?> </td>
											<td> <?php echo $row["phone"]; ?> </td>
											<td> <?php echo $status ?> </td>
											<td><a href = "ngodashboard.php?action=approve&uid=<?php echo $user_id; ?>&nid=<?php echo $userid; ?>"><span class="text-success">Approve</span></a></td>
											<td><a href = "ngodashboard.php?action=reject&uid=<?php echo $user_id; ?>&nid=<?php echo $userid; ?> "><span class="text-danger">Reject</span></a></td>
											</tr>
											
					<?php
										}
									}
										
								}
							}
							
						?>
					
					</tbody>
			</table>
			
				</div>
			</div>
		</div>
    </section>
	
	<section class="bg-primary">
		<div class="container">
			<div class="row">
			  <div class="col-lg-12 text-center">
				<h2 class="section-heading text-white">Donors</h2>
				<hr class="light my-4">
			  </div>
			</div>
		  </div>
		  <div class="container">
			<div class="row">
			  <div class="col-lg-12 mx-auto text-center">
			  <table class="table" id="t3">
						<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Amount Donated</th>
								</tr>
								
							</thead>
							<tbody>
				<?php 
				$sql = "SELECT * FROM ngo_donor n1, ngo_transaction n2 WHERE n1.ngo_id='$userid' AND n1.transaction_id=n2.transaction_id 
																					AND n2.transaction_status='success'";
				$result = mysqli_query($conn, $sql);
				
				if(mysqli_num_rows($result) > 0){
					while($row = mysqli_fetch_assoc($result)){
						$user_id = $row["user_id"];
						$amount = $row["amount"];
					
						$sql1 = "SELECT * FROM user_profile WHERE user_profile_id = '$user_id'";
						$result1 = mysqli_query($conn, $sql1);
							if(mysqli_num_rows($result1) > 0){
								$row = mysqli_fetch_assoc($result1);
							
			?>		
													<tr>
													<td> <?php echo $row["first_name"].$row["middle_name"]." ".$row["last_name"]; ?> </td>
													<td> <?php echo $row["email"]; ?> </td>
													<td> <?php echo $amount ?> </td>
													</tr>
													
			<?php
												
								
							}
												
						}
								
					}
									
			?>	
							</tbody>
					</table> 
				</div>
			</div>
		</div>
    </section>

    <!--<section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Bulk Upload</h2>
			
	<form method="POST" action="addngob.php" enctype="multipart/form-data">
	
	<div class="form-group">
		<div class="input-group input-file" name="Fichier1">
    		<input type="text" class="form-control" name="ngofile" id="ngofile" placeholder='Choose a file...' />			
            <span class="input-group-btn">
        		<button class="btn-sq btn-default btn-lg btn-choose" type="button">Choose</button>
    		</span>
		</div>
	</div>
	
	<div class="form-group">
		<button type="submit" class="btn-sq btn-primary btn-lg pull-right" name="submitb">Submit</button>
		<button type="reset" class="btn-sq btn-danger btn-lg pull-left">Reset</button>
	</div>
	</form>
		
      </div>
    </section>-->

    
	
	<?php include("footer.html"); ?>
	
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="vendor/uploadfile.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
	
	<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
	<script src='jquery.paginate.js'></script>
	
	<script> $('#t1').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>
	<script> $('#t2').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>
	<script> $('#t3').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>
	<script> $('#t4').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>
	
  </body>
</html>

