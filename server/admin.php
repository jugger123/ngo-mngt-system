<?php 

	session_start();
	include("config.php");
	
	if($_SESSION["user_type"] != 'admin'){
		if($_SESSION["user_type"] == 'ngo'){
			//header('location:collrep.php');
		}
		else if($_SESSION["user_type"] == 'admin'){
			header('location:stud.php');
		}
		else{
			header('location:loginhome.php');
		}
	}
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	if(isset($_GET["action"]))
	{
		if($_GET["action"] == "approve")
		{
			$userid = $_GET["aid"];
			
				$sql = "UPDATE user_account SET user_status = 'ACTIVE' WHERE user_profile_id = '$userid'";
				if($conn->query($sql)==true)
				{
					echo '<script>window.location="admin.php"</script>';
				}
				else
				{
					echo '<script>alert("Error")</script>';
					echo '<script>window.location="admin.php"</script>';
				}
				
		}
		if($_GET["action"] == "reject")
		{
			$userid = $_GET["rid"];
			
			$sql = "UPDATE user_account SET user_status = 'INACTIVE' WHERE user_profile_id = '$userid'";
				if($conn->query($sql)==true)
				{
					echo '<script>window.location="admin.php"</script>';
				}
				else
				{
					echo '<script>alert("Error")</script>';
					echo '<script>window.location="admin.php"</script>';
				}
				
		}
	}
	
	include("header.php");
	
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>Admin</title>
	<link href="jquery.paginate.css" rel="stylesheet" type="text/css">

  </head>

  <body id="page-top">

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>NGO FINDER</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Find a deserving NGO and make a difference. #DONATE</p>
            <!--<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>-->
			<form action="ngosearch.php" method="post">
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search NGO" name="searchele"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit" name="search">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
			</form>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="browse">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">List of NGOs</h2>
            <hr class="light my-4">
			
			<table class="table" id="t1">
					<thead>
						<tr>
							<th>Name</th>
							<th>Phone Number</th>
							<th>Email</th>
							<th>Website</th>
							<th>Contact Person</th>
							<th>NGO Status</th>
						</tr>
					</thead>
					<tbody>
					
					<?php 
						
							/*$sql = "SELECT n.name_ngo, n.mobileno, n.email, n.website, n.contactperson, a.user_status 
												FROM ngo_profile n, user_account a WHERE n.user_profile_id=a.user_profile_id";
							*/
							$sql="select * from ngo_profile";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								while($row = mysqli_fetch_assoc($result)){
								$uid=$row["user_profile_id"];
								$name_ngo=$row["name_ngo"];
							$sql2="select user_status from user_account where user_profile_id='$uid'";
							$result2 = mysqli_query($conn, $sql2);
							$row2 = mysqli_fetch_assoc($result2);
					
					?>
									<tr>
									<td> <?php echo "<a style='color:black;'href='ngodetails.php?id=$uid&name=$name_ngo'>"
												.$name_ngo."</a>"; ?> </td>
									<td> <?php echo $row["mobileno"]; ?> </td>
									<td> <?php echo $row["email"]; ?> </td>
									<td> <a style='color:black;' href='http://<?php echo $row["website"]; ?>'>
									     <?php echo $row["website"];?></a></td>
												
									<td> <?php echo $row["contactperson"]; ?> </td>
									<td> <?php  echo $row2["user_status"]; ?> </td>
								
									</tr>
						<?php
								}
							}
							
						?>
					
					</tbody>
			</table>
			
          </div>
        </div>
      </div>
    </section>

    <section id="pending">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Pending Approval</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
	  <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
				<table class="table" id="t2">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Website</th>
							<th>NGO Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody>
					
					<?php 
						
							$sql = "SELECT n.name_ngo, n.email, n.website, a.user_status, n.user_profile_id 
											FROM ngo_profile n, user_account a WHERE n.user_profile_id=a.user_profile_id AND a.user_status = 'PENDING'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								while($row = mysqli_fetch_assoc($result)){
					?>
									<tr>
									<td> <?php echo $row["name_ngo"]; ?> </td>
									<td> <?php echo $row["email"]; ?> </td>
									<td> <a style='color:black;' href='http://<?php echo $row["website"]; ?>'> <?php echo $row["website"]; ?> </a></td>
									<td> <?php echo $row["user_status"]; ?> </td>
									<td><a href = "admin.php?action=approve&aid=<?php echo $row["user_profile_id"]; ?> "><span class="text-success">Approve</span></a></td>
									<td><a href = "admin.php?action=reject&rid=<?php echo $row["user_profile_id"]; ?> "><span class="text-danger">Reject</span></a></td>
									</tr>
						<?php
								}
							}
							
						?>
					
					</tbody>
				</table>
			</div>
		</div>
	</div>
     
    </section>

    <section class="bg-primary" id="add">
      <div class="container-fluid p-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">ADD NGOs</h2>
					<hr class="my-4">
					
					<div id="ngoprof" style="margin-left:30px; margin-right:30px">
									<form id="ngoform" class="form-horizontal" action="addngo.php" role="form" method="POST" enctype="multipart/form-data"> 
										<table class="table">
										<tbody>
										<tr>
										<td>
											<!--<label for="email" class="col-md-3 control-label">Enter NGO Name</label>-->
											
												<input type="text" class="form-control" name="ngoname" id="ngoname" placeholder="NGO Name">
											
										</td><td>
									
											<!--<label for="email" class="col-md-3 control-label">Email</label>-->
											
												<input type="text" class="form-control" name="nemail" id="nemail" placeholder="Official NGO Email">
										
										</td><td>
										
											<!--<label for="contactp" class="col-md-3 control-label">Aim/Objective</label>-->
										
												<input type="text" class="form-control" name="aim" id="aim" placeholder="Objective">
											
										</td></tr><tr><td>
										
											<!--<label for="firstname" class="col-md-3 control-label">Address</label>-->
										
												<input type="text" class="form-control" name="address" id="address" placeholder="Address">
										
										</td><td>
									
											<!--<label for="city" class="col-md-3 control-label">City</label>-->
											
												<input type="text" class="form-control" name="city" id="city" placeholder="City">
											
										</td><td>
									
											<!--<label for="district" class="col-md-3 control-label">District</label>-->
									
												<input type="text" class="form-control" name="district" id="district" placeholder="District">
										
										</td></tr><tr><td>
										
											<!--<label for="state" class="col-md-3 control-label">State</label>-->
											
												<input type="text" class="form-control" name="state" id="state" placeholder="State">
											
										</td><td>
											
									
											<!--<label for="pincode" class="col-md-3 control-label">Pin Code</label>-->
											
												<input type="number" class="form-control" name="pincode" id="pincode" placeholder="Pin Code">
											
										</td><td>
										
											<!--<label for="mbno" class="col-md-3 control-label">Mobile Number</label>-->
											
												<input type="number" class="form-control" name="mbno" id="mbno" placeholder="Mobile Number">
											
										</td></tr><tr><td>
									
											<!--<label for="phno" class="col-md-3 control-label">Phone Number</label>-->
									
												<input type="number" class="form-control" name="phoneno" id="phoneno" placeholder="Phone (Including Area Code)">
											
										</td><td>
										
											<!--<label for="website" class="col-md-3 control-label">Website</label>-->
										
												<input type="text" class="form-control" name="website" id="website" placeholder="Official NGO Website (Optional)">
											
										</td><td>
										
											<!--<label for="contactp" class="col-md-3 control-label">Contact Person</label>-->
										
												<input type="text" class="form-control" name="contactp" id="contactp" placeholder="Contact Person Name">
											
										</td></tr><tr><td>
										
											<label class="label">Sector</label></td><td colspan="2">
										
											<label class="radio-inline" style="float:left">
												<input type="checkbox" name="sector[]" id="sector" value="Health">Health
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="Education">Education
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="Environment">Environment
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="Children">Children
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="Women">Women
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="Arts & Culture">Arts & Culture
											</label>
										
										</td></tr><tr>
										
									
										<td colspan="2">
										
											<label for="file">Registration Certificate :</label>
												<!--<form action="ngoform.php" method="post" enctype="multipart/form-data">-->					
												<input class="col-md-6 control-label" type="file"  id="rcfile" name="rcfile">
										</td><td >
										
											<label for="file">Financial Details:</label>
												<!--<form action="ngoform.php" method="post" enctype="multipart/form-data">-->					
												<input class="col-md-6 control-label" type="file"  id="fdfile" name="fdfile">
										</td></tr>
										
										<tr><td colspan="3">
										
										<div class="form-group">
											<!-- Button -->                                        
											
												<input type="submit" name="submitngo" id="submitngo" class="btn btn-info" value="Submit" ><i class="icon-hand-right"></i>
												 
											
										</div> </td></tr>
										</tbody>
										</table>
									</form>
					</div>
					
				</div>
			</div>
		</div>
      </div>
    </section>

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Bulk Upload</h2>
		
		<a href="ngo_profile.csv"><input type="button" class="btn btn-default" value="Download Sample File"></a><br>
			
	<form method="POST" action="addngob.php" enctype="multipart/form-data">
	
	<div class="form-group">
		<div class="input-group input-file" name="ngofile">
    		<input type="text" class="form-control" name="ngofile" id="ngofile" placeholder='Choose a file...' />			
            <span class="input-group-btn">
        		<button class="btn-sq btn-default btn-lg btn-choose" type="button">Choose</button>
    		</span>
		</div>
	</div>
	
	<div class="form-group">
		<button type="submit" class="btn-sq btn-primary btn-lg pull-right" name="submitb">Submit</button>
		<button type="reset" class="btn-sq btn-danger btn-lg pull-left">Reset</button>
	</div>
	</form>
		
      </div>
    </section>

	
	<?php include("footer.html"); ?>
	
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
	
	<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
	<script src='jquery.paginate.js'></script>
	
	<script> $('#t1').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>
	<script> $('#t2').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>

  </body>
</html>
