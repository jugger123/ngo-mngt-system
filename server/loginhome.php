<?php
   
	  unset($_SESSION);
      include("login.php");
	  include("header.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
	<!--<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/jquery.js"></script>
	
	<!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<link href="css/creative.css" rel="stylesheet">

	<script type="text/javascript">
           function userValidate(){
		      
			  var email = document.getElementById('uemail').value;
			  var fname = document.getElementById('fname').value;
			  var mname = document.getElementById('mname').value;
			  var lname = document.getElementById('lname').value;
			  var password = document.getElementById('upasswd').value;
			  var countrycode = document.getElementById('ccode').value;
			  var phno = document.getElementById('phno').value;
			  
			  if(email == "" || email == "Email Address"){
			     alert("Email Not Entered");
				 return false;
			  }
			  else if(fname == "" || fname == "First Name"){
				alert("First Name Not Entered");
				return false;
			  }
			  else if(mname == "" || mname == "Middle Name"){
				alert("Middle Name Not Entered");
				return false;
			  }
			  else if(lname == "" || lname == "Last Name"){
				alert("Last Name Not Entered");
				return false;
			  }
			  else if(password == "" || password == "Password"){
			     alert("Password Not Entered");
				 return false;
			  }
			  else if(countrycode == "" || countrycode == "Country Code"){
			     alert("Country Code Not Entered");
				 return false;
			  }else if(phno == "" || phno == "Phone Number"){
			     alert("Phone Number Not Entered");
				 return false;
			  }  
			  else{
			     return true;
			  }
		   
		   }
		   
		   function ngoValidate(){
		      
			  var ngoname = document.getElementById('ngoname').value;
			  var address = document.getElementById('address').value;
			  var city = document.getElementById('city').value;
			  var state = document.getElementById('state').value;
			  var pincode = document.getElementById('pincode').value;
			  var email = document.getElementById('nemail').value;
			  var password = document.getElementById('npasswd').value;
			  var website = document.getElementById('website').value;
			  

			  if(ngoname == "" || ngoname == "NGO Name"){
			     alert("NGO Name Not Entered");
				 return false;
			  }
			  else if(address == "" || address == "Address"){
				alert("Address Not Entered");
				return false;
			  }
			  else if(city == "" || city == "City"){
				alert("City Not Entered");
				return false;
			  }
			  else if(state == "" || state == "State"){
				alert("State Not Entered");
				return false;
			  }
			  else if(password == "" || password == "Password"){
			     alert("Password Not Entered");
				 return false;
			  }
			  else if(email == "" || email == "Email Address"){
			     alert("Email Not Entered");
				 return false;
			  }else if(pincode == "" || pincode == "Pin Code"){
			     alert("Pin Code Not Entered");
				 return false;
			  }  
			  else if(website == "" || website == "Website"){
			     alert("Website Not Entered");
				 return false;
			  }  
			  else{
			     return true;
			  }
		   
		   }
					
		   
	</script>
	<script type="text/javascript">
		   function loginvalidate(){
		   
		   
				var emailid = document.getElementById('login-username').value;
				var passwd = document.getElementById('login-password').value;
				
				if(emailid == "" || emailid == "Email Address"){
					alert("Email Not Entered");
					return false;
				}else if(passwd == "" || passwd == "Password"){
					alert("Password Not Entered");
					return false;
				}else{
					return true;
				}
		   }
        </script>
	
  </head>
  <body>
  
  <?php if (isset($_SESSION["error"])): ?>
    <div class="form-errors">
        <?php foreach($_SESSION["error"] as $error): ?>
            <h4 align="center" style="color:red" ><?php echo $error ?></h4>
        <?php endforeach; session_unset(); session_destroy(); ?>
    </div>
  <?php endif; ?> 
  
  <?php if (isset($_SESSION["message"])): ?>
    <div class="form-errors">
        <?php foreach($_SESSION["message"] as $msg): ?>
            <h4 align="center" style="color:red" ><?php echo $msg ?></h4>
        <?php endforeach; session_unset(); Session_destroy(); ?>
    </div>
  <?php endif; ?> 
  
  <header class="masthead text-center text-white d-flex" style="margin-top:0px">

   <div class="container" style="margin-left:30%; margin-top:0%">  
		<!--<a class="navbar-brand js-scroll-trigger" style="margin-left:-1200px; margin-top:10px; color:white;" href="index.php#page-top">NGO Management System</a> -->  
        <div id="loginbox" style="margin-top:100px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title" style="padding-bottom:20px">Sign In</div>
                        <!--<div style="float:right; font-size: 80%; position: relative; top:-10px;"><a style="color:black" href="index.php">Home</a></div>-->
                    </div>     

                    <div style="padding-top:10px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginform" class="form-horizontal" role="form" method="POST" action="login.php" onsubmit="return loginvalidate()">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="login-username" type="email" class="form-control" name="username" placeholder="Email Address">                                        
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    

                                
								<div class="input-group">
                                      <div class="checkbox">
                                        <label style="">
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
								</div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <button id="btn-login" type="submit" name="submit" class="btn btn-success"> Login  </button>
									  <!--<a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>-->
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Don't have an account! 
                                        <a href="#" style="color:black" onClick="$('#loginbox').hide(); $('#userbox').hide(); $('#ngobox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>    
                            </form>     
                        </div>                     
                    </div>  
        </div>
        <div id="signupbox" style="display:none; margin-top:100px;" class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info" style="margin-top:10px">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" style="color:black" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                        </div>  
                        <div class="panel-body" >
                            <!--<form id="signupform" class="form-horizontal" role="form">-->
							
                                <!--<div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>-->
								
								<div class="row" style="margin-top:30px">
									<label for="type" class="col-md-6 control-label">Account Type</label>
									<div class="col-md-6">
									<label class="radio-inline">
										<input type="radio" name="optradio" id="user" value="user" onclick="$('#userbox').show(); $('#ngobox').hide();"><span style="margin-left:5px">User</span>
									</label>
									<label class="radio-inline" style="margin-left:20px">
										<input type="radio" name="optradio" id="ngo" value="ngo" onclick="$('#ngobox').show(); $('#userbox').hide();"><span style="margin-left:5px">NGO</span>
									</label>
									</div>
								</div><br>
								
								<div id="userbox" style="margin-left:70px">
									<form id="userform" class="form-horizontal" role="form" method="POST" action="register.php" onsubmit="return userValidate()"> 
										<div class="form-group">
											<!--<label for="email" class="col-md-3 control-label">Email</label>-->
											<div class="col-md-10">
												<input type="email" class="form-control" name="uemail" id="uemail" placeholder="Email Address">
											</div>
										</div>
											
										<div class="form-group">
											<!--<label for="firstname" class="col-md-3 control-label">First Name</label>-->
											<div class="col-md-10">
												<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name">
											</div>
										</div>
										<div class="form-group">
											<!--<label for="middlename" class="col-md-3 control-label">Middle Name</label>-->
											<div class="col-md-10">
												<input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name">
											</div>
										</div>
										<div class="form-group">
											<!--<label for="lastname" class="col-md-3 control-label">Last Name</label>-->
											<div class="col-md-10">
												<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name">
											</div>
										</div>
										<div class="form-group">
											<!--<label for="password" class="col-md-3 control-label">Password</label>-->
											<div class="col-md-10">
												<input type="password" class="form-control" name="upasswd" id="upasswd" placeholder="Password">
											</div>
										</div>
											
										<div class="form-group">
											<!--<label for="ccode" class="col-md-3 control-label">Country Code</label>-->
											<div class="col-md-10">
												<input type="number" size="3" class="form-control" name="ccode" id="ccode" placeholder="Country Code">
											</div>
										</div>
										<div class="form-group">
											<!--<label for="phonenumber" class="col-md-3 control-label">Phone Number</label>-->
											<div class="col-md-10">
												<input type="number" class="form-control" name="phno" id="phno" placeholder="Phone Number">
											</div>
										</div>
										<div class="form-group">
											<!-- Button -->                                        
											<div class="col-md-offset-3 col-md-10">
												<button id="ubtn-signup" type="submit" name="usubmit" class="btn btn-info"><i class="icon-hand-right"></i> Sign Up</button>
												<!--<span style="margin-left:8px;">or</span>-->  
											</div>
										</div>
									</form>
								</div>
								<div id="ngobox" style="margin-left:-300px; margin-right:-300px">
									<form id="ngoform" class="form-horizontal" role="form" method="POST" action="ngoregister.php" onsubmit="return ngoValidate()" enctype="multipart/form-data"> 
										<table class="table">
										<tbody>
										<tr>
										<td>
											<!--<label for="email" class="col-md-3 control-label">Enter NGO Name</label>-->
											
												<input type="text" class="form-control" name="ngoname" id="ngoname" placeholder="NGO Name">
											
										</td><td>
									
											<!--<label for="email" class="col-md-3 control-label">Email</label>-->
											
												<input type="text" class="form-control" name="nemail" id="nemail" placeholder="Official NGO Email">
										
										</td><td>
										
											<!--<label for="password" class="col-md-3 control-label">Password</label>-->
											
												<input type="password" class="form-control" name="npasswd" id="npasswd" placeholder="Password">
											
										</td></tr><tr><td>
										
											<!--<label for="firstname" class="col-md-3 control-label">Address</label>-->
										
												<input type="text" class="form-control" name="address" id="address" placeholder="Address">
										
										</td><td>
									
											<!--<label for="city" class="col-md-3 control-label">City</label>-->
											
												<input type="text" class="form-control" name="city" id="city" placeholder="City">
											
										</td><td>
									
											<!--<label for="district" class="col-md-3 control-label">District</label>-->
									
												<input type="text" class="form-control" name="district" id="district" placeholder="District">
										
										</td></tr><tr><td>
										
											<!--<label for="state" class="col-md-3 control-label">State</label>-->
											
												<input type="text" class="form-control" name="state" id="state" placeholder="State">
											
										</td><td>
											
									
											<!--<label for="pincode" class="col-md-3 control-label">Pin Code</label>-->
											
												<input type="number" class="form-control" name="pincode" id="pincode" placeholder="Pin Code">
											
										</td><td>
										
											<!--<label for="mbno" class="col-md-3 control-label">Mobile Number</label>-->
											
												<input type="number" class="form-control" name="mbno" id="mbno" placeholder="Mobile Number">
											
										</td></tr><tr><td>
									
											<!--<label for="phno" class="col-md-3 control-label">Phone Number</label>-->
									
												<input type="number" class="form-control" name="phoneno" id="phoneno" placeholder="Phone (Including Area Code)">
											
										</td><td>
										
											<!--<label for="website" class="col-md-3 control-label">Website</label>-->
										
												<input type="text" class="form-control" name="website" id="website" placeholder="Official NGO Website (Optional)">
											
										</td><td>
										
											<!--<label for="contactp" class="col-md-3 control-label">Contact Person</label>-->
										
												<input type="text" class="form-control" name="contactp" id="contactp" placeholder="Contact Person Name">
											
										</td></tr><tr><td>
										
											<label class="label">Sector</label></td><td colspan="2">
										
											<label class="radio-inline" style="float:left">
												<input type="checkbox" name="sector[]" id="sector" value="health">Health
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Education
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Environment
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Children
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Women
											</label>
											<label class="radio-inline" style="padding-left:10px; float:left;">
												<input type="checkbox" name="sector[]" id="sector" value="education">Arts & Culture
											</label>
										
										</td></tr><tr><td>
										
											<!--<label for="contactp" class="col-md-3 control-label">Aim/Objective</label>-->
										
												<input type="text" class="form-control" name="aim" id="aim" placeholder="Objective">
									
										</td><td colspan="2">
										
											<label for="file" class="label col-md-3">Registration Certificate :</label>
												<!--<form action="ngoform.php" method="post" enctype="multipart/form-data">-->					
												<input class="col-md-6 control-label" type="file"  id="rfile" name="rfile" required>
										</td></tr><tr><td colspan="3">
										
										<div class="form-group">
											<!-- Button -->                                        
											
												<button id="btn-signup" type="submit" name="nsubmit" class="btn btn-info"><i class="icon-hand-right"></i> Sign Up</button>
												<!--<span style="margin-left:8px;">or</span>-->  
											
										</div> </td></tr>
										</tbody>
										</table>
									</form>
								</div>
                                
                                <!-- <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">
                                    
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                                    </div>                                          
                                        
                                </div> -->
							
                            <!--</form>-->
                         </div>
                    </div>
                
         </div> 
    </div>
	</header>
	
	</body>
</html>

<?php include("footer.html"); ?>
    