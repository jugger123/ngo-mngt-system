<?php

	session_start();
	include("config.php");
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	
	if(!isset($_SESSION["user_id"]))
	{
		echo '<script> alert("Please login to continue"); </script>';
		echo '<script> window.location="loginhome.php"; </script>';
		
	}

	include("header.php");
	
	$status=$_POST["status"];
	$firstname=$_POST["firstname"];
	$amount=$_POST["amount"];
	$txnid=$_POST["txnid"];

	$posted_hash=$_POST["hash"];
	$key=$_POST["key"];
	$productinfo=$_POST["productinfo"];
	$email=$_POST["email"];
	$salt="pmM7IMGL65";
	
	// Salt should be same Post Request 

	If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
	} else {
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
         }
		 
		 $hash = hash("sha512", $retHashSeq);
  
       if ($hash != $posted_hash) {
	       echo "Invalid Transaction. Please try again";
		} 
		else 
		{
			$timestamp = time();
			
			if(isset($_SESSION["user_id"]))
			{
				$user_id = $_SESSION["user_id"];
		
				$conn=mysqli_connect($host,$username,$password,$db_name);
				if($conn->connect_error){
					die("Connection Error: ". $conn->connect_error);
				}
				
				$sql = "INSERT INTO ngo_transaction VALUES ('$txnid', '', '', '$status', '$amount', '$timestamp')";
				
				if($conn->query($sql)==true)
				{
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Failure Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">
              <strong>Transaction Failed</strong>
            </h3>
            <hr>
		  </div>
		  <div class="col-lg-8 mx-auto">
			<a href="index.php" class="btn btn-primary" role="button">Please click here to try again </a>
		  </div>
		</div>
	</div>
</header>		
					
<?php
					
				}
				else
				{
					echo '<script>alert("Transaction Failed")</script>';
					echo '<script>window.location="index.php"</script>';
				}
				
				mysqli_close($conn);
				
			}
			
		} 
		
		unset($_POST);
	
?>

	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>