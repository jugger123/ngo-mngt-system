<?php
		
	session_start();
	include("config.php");
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	$target_dir = "img/";
	
	if(isset($_POST['submite']))
	{
		$target_file = $target_dir . basename($_FILES["eimage"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$check = getimagesize($_FILES["eimage"]["tmp_name"]);
		$message = "";
		if($check !== false) {
			$uploadOk = 1;
		} else {
			$message = "File is not an image.";
			$uploadOk = 0;
		}
		
		if (file_exists($target_file)) {
			$message = "Sorry, file already exists.";
			$uploadOk = 0;
		}
		
		// Check file size
		if ($_FILES["eimage"]["size"] > 500000) {
			$message = "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
						&& $imageFileType != "gif" ) {
			$message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		
		if ($uploadOk == 0) {
			echo $message;
			//echo '<script> window.location="ngodashboard.php#events"; </script>';
		} 
		else {
			if (move_uploaded_file($_FILES["eimage"]["tmp_name"], $target_file)) {
				$imagename=basename( $_FILES["eimage"]["name"]);	
				
				$user_id = $_SESSION["user_id"];
				$name = $_POST['ename'];
				$date = $_POST['edate'];
				$time = $_POST['etime'];
				$place = $_POST['eplace'];
				$description = $_POST['edesc'];
				
				$today = getdate();
				$year = $today['year'];

				$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
				$event_id = $year . $salt1;	
				
				$sql = "INSERT INTO ngo_events VALUES ('$user_id', '$event_id', '$name', '$date', '$time', '$place', '$description', '$imagename')";
				
				if($conn->query($sql) == true)
				{
					echo '<script> alert("Event added") </script>';
					echo '<script> window.location="ngodashboard.php#events"; </script>';
				}
				else
				{
					echo '<script> alert("Failed") </script>';
					echo '<script> window.location="ngodashboard.php#events"; </script>';
				}
				
			} else {
				echo '<script> alert("Error uploading file") </script>';
				echo '<script> window.location="ngodashboard.php#events"; </script>';
			}
		}
			
		
		unset($_POST);
	}
	
	if(isset($_POST['submitp']))
	{
		$target_file = $target_dir . basename($_FILES["pimage"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$check = getimagesize($_FILES["pimage"]["tmp_name"]);
		$message = "";
		if($check !== false) {
			$uploadOk = 1;
		} else {
			$message = "File is not an image.";
			$uploadOk = 0;
		}
		
		if (file_exists($target_file)) {
			$message = "Sorry, file already exists.";
			$uploadOk = 0;
		}
		
		// Check file size
		if ($_FILES["pimage"]["size"] > 500000) {
			$message = "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
						&& $imageFileType != "gif" ) {
			$message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		
		if ($uploadOk == 0) {
			echo '<script> alert(</script>'.$message.'<script>) </script>';
			echo '<script> window.location="ngodashboard.php#events"; </script>';
		} 
		else {
			if (move_uploaded_file($_FILES["pimage"]["tmp_name"], $target_file)) {
				$imagename=basename( $_FILES["pimage"]["name"]);
		
				$user_id = $_SESSION["user_id"];
				$name = $_POST['pname'];
				$price = $_POST['pprice'];
				$year = $_POST['yprod'];
				$description = $_POST['pdesc'];
				
				$today = getdate();
				$year = $today['year'];

				$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
				$product_id = $year . $salt1;	
				
				$sql = "INSERT INTO ngo_products VALUES ('$user_id', '$product_id', '$name', '$price', '$year', '$description', 'ACTIVE', '')";
				
				if($conn->query($sql) == true)
				{
					echo '<script> alert("Product Added") </script>';
					echo '<script> window.location="ngodashboard.php#products"; </script>';
				}
				else
				{
					echo '<script> alert("Failed") </script>';
					echo '<script> window.location="ngodashboard.php#products"; </script>';
				}
			}
			else {
				echo '<script> alert("Error uploading file") </script>';
				echo '<script> window.location="ngodashboard.php#events"; </script>';
			}
		}
		unset($_POST);
	}
	
	if(isset($_POST['submitj']))
	{
		$user_id = $_SESSION["user_id"];
		$position = $_POST['jobp'];
		$description = $_POST['jobdesc'];
		$duration = $_POST['jobdur'];
		$location = $_POST['jobloc'];
		$type = $_POST['optradio'];
		
		$today = getdate();
		$year = $today['year'];

		$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
		$job_id = $year . $salt1;	
		
		$sql = "INSERT INTO ngo_jobs VALUES ('$user_id', '$job_id', '$position', '$description', '$duration', '$location', '$type', 'ACTIVE')";
		
		if($conn->query($sql) == true)
		{
			echo '<script> alert("Job added") </script>';
			echo '<script> window.location="ngodashboard.php#jobs"; </script>';
		}
		else
		{
			echo '<script> alert("Failed") </script>';
			echo '<script> window.location="ngodashboard.php#jobs"; </script>';
		}
		unset($_POST);
	}
	
?>