<?php	
	session_start();
	include("config.php");
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	if(!isset($_SESSION["user_id"]))
	{
		echo '<script> alert("Please login to continue"); </script>';
		echo '<script> window.location="loginhome.php"; </script>';
	}
	else
	{
		if($_SESSION["user_type"] == "ngo" or $_SESSION["user_type"] == "admin")
		{
			echo '<script> alert("Restricted Access"); history.go(-1);</script>';
		}
		if(isset($_GET["eid"]))
		{
			$event_id = $_GET["eid"];
			$user_id = $_SESSION["user_id"];
			
			$sql = "INSERT INTO event_participants VALUES ('$user_id', '$event_id')";
			
			if($conn->query($sql)==true)
			{
				echo '<script> alert("Participation Successful"); history.go(-1); </script>';
			}
			else
			{
				echo '<script> alert("Request Failed"); history.go(-1); </script>';
			}
		}
	}
	
?>	