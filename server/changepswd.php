<?php
   
	  session_start();
	  include("config.php");
	  include("header.php");
	  
	if(isset($_SESSION["user_id"]))
	{
		if(isset($_POST['submitcp']))
		{
			// Connect to server and select databse.
			$conn=mysqli_connect($host,$username,$password,$db_name);
			if($conn->connect_error){
				die("Connection Error: ". $conn->connect_error);
			}
			
			$user_id = $_SESSION["user_id"];
			$cpswd = $_POST['cpswd'];
			$npswd = $_POST['npswd'];
			
			unset($_POST);
			
			$sql1 = "Select * FROM user_account WHERE user_profile_id = '$user_id'";
			$result = mysqli_query($conn, $sql1);
			
			if(mysqli_num_rows($result) > 0){
				
					$row = mysqli_fetch_assoc($result);
					
					$pass = $row["pswd"];
					$salt1 = $row["pswd_salt"];
					
					$SaltedPass = $salt1 . $cpswd;
					$HashedPass = hash('sha256', $SaltedPass);
				
					// Checking If the calculated hash and stored hash are same or not

					if($HashedPass == $pass){
						
						$salt2 = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
						$saltedpasswd = $salt2 . $npswd;
						$hashpassword = hash('sha256', $saltedpasswd);
						
						$sql2 = "UPDATE user_account SET pswd = '$hashpassword', pswd_salt = '$salt2' WHERE user_profile_id = '$user_id'";
						if($conn->query($sql2)==true)
						{
							echo '<script> alert("Password Changed") </script>';
							if($_SESSION["user_type"] == "ngo")
							{
								echo '<script> window.location="ngodashboard.php"; </script>';
							}
							if($_SESSION["user_type"] == "user")
							{
								echo '<script> window.location="userprofile.php"; </script>';
							}
						}
						else{
							echo '<script> alert("Please try again") </script>';
							echo '<script> window.location="changepswd.php"; </script>';
						}
						
					}
					else{
						echo '<script> alert("Did not match current password") </script>';
						echo '<script> window.location="changepswd.php"; </script>';
					}
				
			}
		}
	}
	else
	{
		echo '<script> window.location="loginhome.php"; </script>';
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
	<!--<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/jquery.js"></script>
	
	<!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	
	<!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
	
	<link href="css/creative.css" rel="stylesheet">
	
	</head>
	<body id="page-top">
	
	<header class="masthead text-center text-white d-flex" style="margin-top:0px">
	
	<div class="container" style="margin-left:32%; margin-top:0%">  
		<!--<a class="navbar-brand js-scroll-trigger" style="margin-left:-1200px; margin-top:10px; color:white;" href="index.php#page-top">NGO Management System</a> -->  
        <div id="loginbox" style="margin-top:100px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <h4><div class="panel-title" style="padding-bottom:20px">Change Password</div></h4>
                        <!--<div style="float:right; font-size: 80%; position: relative; top:-10px;"><a style="color:black" href="index.php">Home</a></div>-->
                    </div>     

                    <div style="padding-top:10px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginform" class="form-horizontal" role="form" method="POST" action="changepswd.php">
                                    
									<div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="current-password" type="password" class="form-control" name="cpswd" placeholder="Current Password">                                        
                                    </div>
                                
									<div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="new-password" type="password" class="form-control" name="npswd" placeholder="New Password">
                                    </div>
                                    
                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <button id="btn-login" type="submit" name="submitcp" class="btn btn-success"> Submit </button>
									  <!--<a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>-->
                                    </div>
                                </div> 
                        </form>     
                    </div>                     
            </div>  
        </div>
        
    </div>
	</header>
	
	</body>
</html>

<?php include("footer.html"); ?>