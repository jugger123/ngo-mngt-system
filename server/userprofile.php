<?php 

	session_start();
	include("config.php");
	include("header.php");
	
	if(isset($_SESSION["user_id"]))
	{
		if($_SESSION["user_type"] != "user"){
			unset($_SESSION);
			header("loginhome.php");
		}
		else
		{
			$userid = $_SESSION["user_id"];
			
			// Connect to server and select databse.
			$conn=mysqli_connect($host,$username,$password,$db_name);
			if($conn->connect_error){
				die("Connection Error: ". $conn->connect_error);
			}
		}
	}
	
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <title>User</title>
	<link href="jquery.paginate.css" rel="stylesheet" type="text/css">

  </head>
  
  <body id="page-top">

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>NGO FINDER</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Find a deserving NGO and make a difference. #DONATE</p>
            <!--<a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>-->
			<form action="ngosearch.php" method="post">
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search NGO" name="searchele"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit" name="search">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
			</form>
          </div>
        </div>
      </div>
    </header>
	
	<section class="bg-primary" id="profile">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">Profile</h2>
            <hr class="light my-4">
			<div id="tab">
			<table class="table" id="t1">
			
					<tbody>
					
					<?php 
						
							$sql = "SELECT * FROM user_profile WHERE user_profile_id = '$userid'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
									<tr>
									<th>First Name</th>
									<td><?php echo $row["first_name"]; ?></td>
									</tr>
									<tr>
									<th>Middle Name</th>
									<td><?php echo $row["middle_name"]; ?></td>
									</tr>
									<tr>
									<th>Last Name</th>
									<td><?php echo $row["last_name"]; ?></td>
									</tr>
									<tr>
									<th>Email</th>
									<td><?php echo $row["email"]; ?></td>
									</tr>
									<tr>
									<th>Phone Number</th>
									<td><?php echo $row["phone"]; ?></td>
									</tr>
									
						<?php
								
							}
							
						?>
									
								
									
					</tbody>
			</table>
			
				<button class="btn btn-info" role="button" onclick="$('#userform').show(); $('#tab').hide();">Edit Details</button>
				<a href="changepswd.php" class="btn btn-info" role="button">Change Password</a>
			</div>
			
			<div id="userform" style="display:none">
			<form id="form" class="form-horizontal" role="form" method="POST" action="profilechange.php" >
										<table class="table">
										<tbody>
										<tr>
										<td>
											<!--<label for="email" class="col-md-3 control-label">Enter NGO Name</label>-->
											
												<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" value="<?php echo $row["first_name"]?>">
											
										</td></tr><tr><td>
									
											<!--<label for="email" class="col-md-3 control-label">Email</label>-->
											
												<input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name" value="<?php echo $row["middle_name"]?>">
										
										</td></tr><tr><td>
										
											<!--<label for="firstname" class="col-md-3 control-label">Address</label>-->
										
												<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" value="<?php echo $row["last_name"]?>">
										
										</td></tr><tr><td>
									
											<!--<label for="city" class="col-md-3 control-label">City</label>-->
											
												<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $row["email"]?>">
											
										</td></tr><tr><td>
									
											<!--<label for="district" class="col-md-3 control-label">District</label>-->
									
												<input type="number" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $row["phone"]?>">
										
										</td></tr><tr>
										
										<td colspan="3">
										
										<div class="form-group">                                       
											
												<button id="btn-signup" type="submit" name="uchange" class="btn btn-info"><i class="icon-hand-right" onclick="$('#ngoform').hide(); $('#tab').show();"></i> Save Changes</button>
											
										</div> </td></tr>
										</tbody>
										</table>
									</form>
									<button class="btn btn-info" role="button" onclick="$('#userform').hide(); $('#tab').show();"><i class="icon-hand-right"></i> Cancel</button>
							</div>
          </div>
        </div>
      </div>
    </section>
	
	<?php include("footer.html"); ?>
	
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="vendor/uploadfile.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
	
	<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
	<script src='jquery.paginate.js'></script>
	
	<!--<script> $('#t1').paginate({'elemsPerPage': 5, 'maxButtons': 5 }); </script>-->
	
  </body>
</html>