<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php#page-top">NGO Management System</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
		
		<?php 
		
			if(isset($_SESSION["user_id"]))
			{
				if($_SESSION["user_type"]=="admin")
				{
					
		?>
		
				 <ul class="navbar-nav ml-auto">
					<li class="nav-item">
					  <a class="nav-link js-scroll-trigger" href="admin.php#page-top">Dashboard</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link js-scroll-trigger" href="admin.php#browse">Browse NGOs</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link js-scroll-trigger" href="admin.php#pending">Pending Requests</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link js-scroll-trigger" href="admin.php#add">Add NGOs</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link js-scroll-trigger" href="signout.php">Signout</a>
					</li>
				</ul>
		
				
		<?php
				}
				else if($_SESSION["user_type"]=="ngo")
				{
		?>
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
						  <a class="nav-link js-scroll-trigger" href="ngodashboard.php#page-top">Dashboard</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link js-scroll-trigger" href="ngodashboard.php#profile">Profile</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link js-scroll-trigger" href="ngodashboard.php#add">Add Services</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link js-scroll-trigger" href="ngodashboard.php#list">User List</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link js-scroll-trigger" href="signout.php">Signout</a>
						</li>
					</ul>
		<?php
				}
				else
				{
		?>
				<ul class="navbar-nav ml-auto">
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#page-top">Home</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#about">About</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#services">Services</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#portfolio">Portfolio</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#contact">Contact</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="userprofile.php">Profile</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="signout.php">Signout</a>
				</li>
			  </ul>
			  
		<?php
					
				}
			}
			else
			{ 
		?>
		
			  <ul class="navbar-nav ml-auto">
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#page-top">Home</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#about">About</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#services">Services</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#portfolio">Portfolio</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="index.php#contact">Contact</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link js-scroll-trigger" href="loginhome.php">Login</a>
				</li>
			  </ul>
			  <?php
			}
		?>
        </div>
      </div>
    </nav>
	
	</body>
</html>