<?PHP
		session_start();
		include("config.php");
		include("header.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Event Details Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">
              <strong><?php $ename=$_GET["name"];echo $_GET["name"]; ?> </strong>
            </h3>
            <hr>
			<a href="eventpart.php?eid=<?php echo $_GET["eid"];?>" class="btn btn-primary" role="button">Participate</a>
		  </div>
		</div>
	</div>
</header>


<section class="bg-primary" id="event">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">Event Details</h2>
            <hr class="light my-4">
			
<?php

	$conn = mysqli_connect($host,$username,$password,$db_name);
	$page=null;
	if(isset($_GET["id"])) {
		$ele=$_GET["id"];
	}
     $_SESSION['search_text']= $ele;
	 if($ele == null)
		   {
		   		echo "<script> alert('please enter text')
				history.go(-1);</script>";
				//header('Location:home.php');
		   }
	else{
						
							$sql = "SELECT * FROM ngo_events WHERE user_profile_id = '$ele' and event_name='$ename'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
								
							<table class="table">	
								<tbody>
									<tr>
									<th>About</th>
									<td><?php echo $row["about"]; ?></td>
									</tr>
									<tr>
									<th>Place</th>
									<td><?php echo $row["event_place"]; ?></td>
									</tr>
									<tr>
									<th>Date</th>
									<td><?php echo $row["event_date"]; ?></td>
									</tr>
									<tr>
									<th>Time</th>
									<td><?php echo $row["event_time"]; ?></td>
									</tr>
									
								</tbody>
						</table>	
						<?php
								
							}
							else
							{
						?>
							<h3>** No Details are present at the moment **</h3>
							
					<?php
							}
						}  
					?>
			
          </div>
        </div>
      </div>
    </section>
	
	<section id="profile">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading">Conducting NGO Profile</h2>
            <hr class="my-4">
			
<?php

	$conn = mysqli_connect($host,$username,$password,$db_name);
	$page=null;
	if(isset($_GET["id"])) {
		$ele=$_GET["id"];
	}
     $_SESSION['search_text']= $ele;
	 if($ele == null)
		   {
		   		echo "<script> alert('please enter text')
				history.go(-1);</script>";
				//header('Location:home.php');
		   }
	else{
?>

					
					<?php 
						
							$sql = "SELECT * FROM ngo_profile WHERE user_profile_id = '$ele'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
								
							<table class="table">	
								<tbody>
									<tr>
									<th>Name</th>
									<td><?php echo $row["name_ngo"]; ?></td>
									</tr>
									<tr>
									<th>Address</th>
									<td><?php echo $row["address"]; ?></td>
									</tr>
									<tr>
									<th>City</th>
									<td><?php echo $row["city"]; ?></td>
									</tr>
									<tr>
									<th>District</th>
									<td><?php echo $row["district"]; ?></td>
									</tr>
									<tr>
									<th>State</th>
									<td><?php echo $row["state"]; ?></td>
									</tr>
									<tr>
									<th>Pincode</th>
									<td><?php echo $row["pincode"]; ?></td>
									</tr>
									<tr>
									<th>Mobile Number</th>
									<td><?php echo $row["mobileno"]; ?></td>
									</tr>
									<tr>
									<th>Phone Number</th>
									<td><?php echo $row["phoneno"]; ?></td>
									</tr>
									<tr>
									<th>Email</th>
									<td><?php echo $row["email"]; ?></td>
									</tr>
									<tr>
									<th>Website</th>
									<td><?php echo $row["website"]; ?></td>
									</tr>
									<tr>
									<th>Contact Person</th>
									<td><?php echo $row["contactperson"]; ?></td>
									</tr>
									<tr>
									<th>Sector</th>
									<td><?php echo $row["sector"]; ?></td>
									</tr>
									<tr>
									<th>Aim/Objective</th>
									<td><?php echo $row["aim"]; ?></td>
									</tr>
								</tbody>
						</table>	
						<?php
								
							}
							else
							{
						?>
							<h3>** Financials not present at the moment **</h3>
							
					<?php
							}
					}  
					?>
			
          </div>
        </div>
      </div>
    </section>
	
<script>
	function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}
</script>
	
	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>