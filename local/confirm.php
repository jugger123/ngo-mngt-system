<?php

	session_start();
	include("config.php");
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	
	if(!isset($_SESSION["user_id"]))
	{
		echo '<script> alert("Please login to continue"); </script>';
		echo '<script> window.location="loginhome.php"; </script>';
		
	}

	include("header.php");
	
	if(isset($_POST["submitd"]))
	{
		$user_id = $_SESSION["user_id"];
		$ngo_id = $_POST["ngoid"];
		$ngo_name = $_POST["ngoname"];
		$amount = $_POST["donate"];
		
		$today = getdate();
		$year = $today['year'];

		$salt = substr(hash('sha256', mt_rand() . microtime()), 0, 20); 
		$txn_id = $year . $salt;
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Confirm Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">
              <strong>Confirm Details</strong>
            </h3>
            <hr>
		  </div>
		  <div class="col-lg-8 mx-auto">
		  <div class="table-responsive">
			<table class="table">
				<thead>
					<th>Transaction ID</th>
					<th>NGO Name</th>
					<th>Amount</th>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $txn_id; ?></td>
						<td><?php echo $ngo_name; ?></td>
						<td><?php echo $amount; ?></td>
					</tr>
				</tbody>
			</table>
			</div>
			<form action="PayUMoney_form.php" method="POST">
					<input type="hidden" name="ngo_id" value="<?php echo $ngo_id; ?>">
					<input type="hidden" name="txn_id" value="<?php echo $txn_id; ?>">
					<input type="hidden" name="amount" value="<?php echo $amount; ?>"><br>
					<button type="submit" class="btn btn-success" name="checkout">Confirm & Checkout</button>
				</form>
		  </div>
		</div>
	</div>
</header>

<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>