<?php

session_start();

include("config.php");

// Connect to server and select databse.
$conn=mysqli_connect($host,$username,$password,$db_name);
if($conn->connect_error){
	die("Connection Error: ". $conn->connect_error);
}

if(isset($_POST['usubmit'])){
	
	$email = $_POST['uemail'];
	$firstname = $_POST['fname'];
	$lastname = $_POST['lname'];
	$middlename = $_POST['mname'];
	$password = $_POST['upasswd'];
	$countrycode = $_POST['ccode'];
	$phoneno = $_POST['phno'];
	
	$sql = "SELECT * FROM user_account WHERE email = '$email'";
	$result = mysqli_query($conn, $sql);
	
	if(mysqli_num_rows($result) > 0)
	{
		$_SESSION['message'] = array("Account already exists"); 
		header("location:loginhome.php");
	}
	else
	{
		$today = getdate();
		$year = $today['year'];

		$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
		$user_id = $year . $salt1;													// Creating user profile id
		
		// Hashing Password
		
		$salt2 = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$saltedpasswd = $salt2 . $password;
		$hashpassword = hash('sha256', $saltedpasswd);

		$sql1 = "INSERT INTO user_profile VALUES ('$user_id', '$firstname', '$middlename', '$lastname', '$email', '$countrycode', '$phoneno')";
		$sql2 = "INSERT INTO user_account VALUES ('$user_id', '$email', '$hashpassword', '$salt2', 'sha256', 'user', 'EMAIL VERIFICATION', 'N')";
		
		if($conn->query($sql1)==true && $conn->query($sql2)==true){
			
			$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?id=" . $user_id;
				$toEmail = $email;
				$subject = "User Registration Activation Email";
				$content = "Click this link to activate your account " 
								. $actual_link ;
				$mailHeaders = "From: Admin\r\n";
				if(mail($toEmail, $subject, $content, $mailHeaders)) {
					$_SESSION['message'] = array("Email with verification link has been sent"); 
					header("location:loginhome.php");
				}else{
					$_SESSION['error'] = array("Could not sent email");
					header("location:loginhome.php");
				}
			
				unset($_POST);
			
		}else{
			$_SESSION['error'] = array("Could not complete registration");
			header("location:loginhome.php");
		}
	}
}

if(!empty($_GET["id"]))
	{
		$userid = $_GET["id"];
		
		$sql1 = "SELECT * FROM user_account WHERE user_profile_id = '$userid'";
		$result = mysqli_query($conn, $sql1);
		
		if(mysqli_num_rows($result) > 0)
		{
			while($row = mysqli_fetch_assoc($result)){
				$status = $row["user_status"];
			}
			if($status == 'ACTIVE')
			{
				echo '<script> alert("Account already activated") </script>';
				echo '<script> window.location="loginhome.php"; </script>';
			}
			else
			{
				$sql2 = "UPDATE user_account SET user_status='ACTIVE' WHERE user_profile_id = '$userid'";
				if($conn->query($sql2)=== true)
				{
					echo '<script> alert("Account Activated") </script>';
					echo '<script> window.location="loginhome.php"; </script>';
				}
				else
				{
					echo '<script> alert("Problem in account activation") </script>';
				}
			}
		}
	}

?>