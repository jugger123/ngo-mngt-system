<!DOCTYPE html>
<html lang="en">
<head>

	<title>Events List</title>
 
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link href="jquery.paginate.css" rel="stylesheet" type="text/css">
 
	<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
	
</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">
              <strong>Jobs</strong>
            </h3>
            <hr>
		  </div>
		  <div class="col-lg-8 mx-auto">
			<form action="ngosearch.php" method="post">
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search NGO" name="searchele" />
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit" name="search">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
			</form><br>
          </div>
		  <div class="col-lg-8 mx-auto">
		 

<?php
		header('Cache-Control: no cache'); //no cache
		session_cache_limiter('private_no_expire'); // works
		//session_cache_limiter('public'); // works too
	
	session_start();
	include("config.php");
	include("header.php");
	
	$conn = mysqli_connect($host,$username,$password,$db_name);
		
		// based on Project name
		
		$query1="select * from ngo_jobs";
		       //  or sector like '%$ele%'";
		
		$results1 = mysqli_query($conn,$query1);
		
		?>
		 <table class="table">
		  <tbody>
<?php
			
			while ($row1 = mysqli_fetch_array($results1)) {
				$uid=$row1['user_profile_id'];
				$job_id = $row1['job_id'];
					$query2="select * from ngo_profile where user_profile_id='$uid'";
					$results2 = mysqli_query($conn,$query2);
					$row2 = mysqli_fetch_array($results2);
			
					$job_position=$row1['job_position'];
					
				
				
?>
		<tr><td>		
 <div class="w3-card-4" style="margin-left:20%; margin-right:30%;">
    <header class="w3-container" style="background-color:#F05F40">
      <h2 style="color:white" ><?php echo $row1['job_position']; ?></h2>
    </header>

    <div class="w3-container">
      <br/><p><?php echo $row2["name_ngo"]; ?></p>
	  <p><?php echo $row1["job_description"]; ?></p>
    </div>

    <footer class="w3-container w3-center">
      <h4> <?php echo "<a href='jobdetails.php?id=$uid&jid=$job_id&name=$job_position' role='button' class='btn btn-info'>View</a>";?></h4>
    </footer>
  </div><br/></td></tr>
  
<?php
			}
?>

</tbody></table>
		</div>
       </div>
	</div>
</header>

	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

	<script src='http://code.jquery.com/jquery-1.12.4.min.js'></script>
	<script src='jquery.paginate.js'></script>
	<script> $('table').paginate({'elemsPerPage': 1, 'maxButtons': 5 }); </script>

</body>
<?php include("footer.html"); ?>
</html>