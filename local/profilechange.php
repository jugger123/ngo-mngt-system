<?php

	session_start();
	include("config.php");
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	if(isset($_POST["nchange"]))
	{
		$ngoname = $_POST['ngoname'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$district = $_POST['district'];
		$state = $_POST['state'];
		$pincode = $_POST['pincode'];
		$mobileno = $_POST['mbno'];
		$phoneno = $_POST['phoneno'];
		$email = $_POST['nemail'];
		$website = $_POST['website'];
		$contactperson = $_POST['contactp'];
		$aim = $_POST['aim'];
		$sect = $_POST['sector'];
		
		$user_id = $_SESSION["user_id"];
		$sec = "";
			foreach($sect as $sector)
			{
				$sec .= $sector . ",";
			}
		
		$sql = "UPDATE ngo_profile SET name_ngo='$ngoname', address='$address', city='$city', district='$district', state='$state', pincode='$pincode',
													mobileno='$mobileno', phoneno='$phoneno', email=' $email', website='$website', contactperson='$contactperson',
													sector='$sec', aim='$aim' WHERE user_profile_id='$user_id'";
													
		if($conn->query($sql) == true)
		{
			echo '<script> alert("Details Changed"); history.go(-1); </script>';
		}
		else
		{
			echo '<script> alert("Failed"); history.go(-1); </script>';
		}
	}
	
	if(isset($_POST["uchange"]))
	{
		$fname = $_POST['fname'];
		$mname = $_POST['mname'];
		$lname = $_POST['lname'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		
		$user_id = $_SESSION["user_id"];
		
		$sql = "UPDATE user_profile SET first_name='$fname', middle_name='$mname', last_name='$lname', email='$email', phone='$phone' WHERE user_profile_id='$user_id'";
													
		if($conn->query($sql) == true)
		{
			echo '<script> alert("Details Changed"); history.go(-1); </script>';
		}
		else
		{
			echo '<script> alert("Failed"); history.go(-1); </script>';
		}
	}
?>