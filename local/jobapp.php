<?php
	
	session_start();
	include("config.php");
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	$target_dir = "resume/";
	if(isset($_POST["submitr"]))
	{
		$target_file = $target_dir . basename($_FILES["rfile"]["name"]);
		$uploadOk = 1;
		$message = "";
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		/*$check = getimagesize($_FILES["eimage"]["tmp_name"]);
	
		if($check !== false) {
			$uploadOk = 1;
		} else {
			$message = "File is not an image.";
			$uploadOk = 0;
		}*/
		
		if (file_exists($target_file)) {
			$message = "Sorry, file already exists.";
			$uploadOk = 0;
		}
		
		// Check file size
		if ($_FILES["rfile"]["size"] > 5000000) {
			$message = "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		
		if($imageFileType != "pdf") {
			$message = "Sorry, only PDF files are allowed.";
			$uploadOk = 0;
		}
		
		if ($uploadOk == 0) {
			echo $message;
			//echo '<script> window.location="ngodashboard.php#events"; </script>';
		} 
		else {
			if (move_uploaded_file($_FILES["rfile"]["tmp_name"], $target_file)) {
				$imagename=basename( $_FILES["rfile"]["name"]);	
				
				$user_id = $_SESSION["user_id"];
				$job_id = $_POST['jobid'];
				
				$today = time();
				$date = date("Y-m-d",$today);
				
				$sql = "INSERT INTO ngo_jobs_apply VALUES ('$user_id', '$job_id', '$date', '$imagename')";
				
				if($conn->query($sql) == true)
				{
					echo '<script> alert("Applied"); history.go(-2) </script>';
				}
				else
				{
					echo '<script> alert("Failed"); history.go(-1); </script>';
				}
				
			} else {
				echo '<script> alert("Error uploading file");  </script>';
			}
		}
		unset($_POST);
	}
	
?>