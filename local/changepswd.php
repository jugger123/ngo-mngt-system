<?php
   
	  session_start();
	  include("config.php");
	  include("header.php");
	  
	if(isset($_SESSION["user_id"]))
	{
		if(isset($_POST['submitcp']))
		{
			// Connect to server and select databse.
			$conn=mysqli_connect($host,$username,$password,$db_name);
			if($conn->connect_error){
				die("Connection Error: ". $conn->connect_error);
			}
			
			$user_id = $_SESSION["user_id"];
			$cpswd = $_POST['cpswd'];
			$npswd = $_POST['npswd'];
			
			unset($_POST);
			
			$sql1 = "Select * FROM user_account WHERE user_profile_id = '$user_id'";
			$result = mysqli_query($conn, $sql1);
			
			if(mysqli_num_rows($result) > 0){
				
					$row = mysqli_fetch_assoc($result);
					
					$pass = $row["pswd"];
					$salt1 = $row["pswd_salt"];
					
					$SaltedPass = $salt1 . $cpswd;
					$HashedPass = hash('sha256', $SaltedPass);
				
					// Checking If the calculated hash and stored hash are same or not

					if($HashedPass == $pass){
						
						$salt2 = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
						$saltedpasswd = $salt2 . $npswd;
						$hashpassword = hash('sha256', $saltedpasswd);
						
						$sql2 = "UPDATE user_account SET pswd = '$hashpassword', pswd_salt = '$salt2' WHERE user_profile_id = '$user_id'";
						if($conn->query($sql2)==true)
						{
							echo '<script> alert("Password Changed") </script>';
							if($_SESSION["user_type"] == "ngo")
							{
								echo '<script> window.location="ngodashboard.php"; </script>';
							}
							if($_SESSION["user_type"] == "user")
							{
								echo '<script> window.location="userprofile.php"; </script>';
							}
						}
						else{
							echo '<script> alert("Please try again") </script>';
							echo '<script> window.location="changepswd.php"; </script>';
						}
						
					}
					else{
						echo '<script> alert("Did not match current password") </script>';
						echo '<script> window.location="changepswd.php"; </script>';
					}
				
			}
		}
	}
	else
	{
		echo '<script> window.location="loginhome.php"; </script>';
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head> 

	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/jquery.js"></script>
	
	</head>
	
	<body id="page-top">
	
	<header class="masthead text-center text-white d-flex" style="margin-top:0%">
	
	<div class="container" style="margin-top:0%">  
	<center>  
        <div id="loginbox" style="margin-top:5%;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <h4><div class="panel-title" style="padding-bottom:20px">Change Password</div></h4>
                       
                    </div>     

                    <div style="padding-top:10px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginform" class="form-horizontal" role="form" method="POST" action="changepswd.php">
                                    
									<div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="current-password" type="password" class="form-control" name="cpswd" placeholder="Current Password">                                        
                                    </div>
                                
									<div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="new-password" type="password" class="form-control" name="npswd" placeholder="New Password">
                                    </div>
                                    
                                <div style="margin-top:10px" class="form-group">

                                    <div class="col-sm-12 controls">
                                      <button id="btn-login" type="submit" name="submitcp" class="btn btn-success"> Submit </button>
                                    </div>
                                </div> 
                        </form>     
                    </div>                     
            </div>  
        </div>
		</center>
        
    </div> 
	</header>
	
	<?php include("footer.html"); ?>
	
	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
	
	</body>
</html>