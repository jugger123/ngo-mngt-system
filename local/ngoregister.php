<?php

session_start();

include("config.php");

// Connect to server and select databse.
$conn=mysqli_connect($host,$username,$password,$db_name);
if($conn->connect_error){
	die("Connection Error: ". $conn->connect_error);
}

if(isset($_POST['nsubmit'])){
	
	$ngoname = $_POST['ngoname'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$district = $_POST['district'];
	$state = $_POST['state'];
	$pincode = $_POST['pincode'];
	$mobileno = $_POST['mbno'];
	$phoneno = $_POST['phoneno'];
	$email = $_POST['nemail'];
	$password = $_POST['npasswd'];
	$website = $_POST['website'];
	$contactperson = $_POST['contactp'];
	$aim = $_POST['aim'];
	$sect = $_POST['sector'];
	
	$sql = "SELECT * FROM user_account WHERE email = '$email'";
	$result = mysqli_query($conn, $sql);
	
	if(mysqli_num_rows($result) > 0)
	{
		$_SESSION['message'] = array("Account already exists"); 
		header("location:loginhome.php");
	}
	else
	{
	
		$today = getdate();
		$year = $today['year'];

		$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
		$user_id = $year . $salt1;	
		
		$salt2 = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$saltedpasswd = $salt2 . $password;
		$hashpassword = hash('sha256', $saltedpasswd);
		
		$sql1 = "INSERT INTO user_account VALUES ('$user_id', '$email', '$hashpassword', '$salt2', 'sha256', 'ngo', 'EMAIL VERIFICATION', 'N')";
		
		if($conn->query($sql1) == true)
		{
			$sec = "";
			foreach($sect as $sector)
			{
				$sec .= $sector . ",";
			}
			//echo $sec;
			
			$target_dir = "rc/";
			
			$target_file = $target_dir . basename($_FILES["rfile"]["name"]);
			$uploadOk = 1;
			$message = "";
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			
			if (file_exists($target_file)) {
				$message = "Sorry, file already exists.";
				$uploadOk = 0;
			}
			
			// Check file size
			if ($_FILES["pfile"]["size"] > 500000) {
				$message = "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			
			if($imageFileType != "pdf") {
				$message = "Sorry, only PDF files are allowed.";
				$uploadOk = 0;
			}
			
			if ($uploadOk == 0) {
				echo $message;
				//echo '<script> window.location="ngodashboard.php#events"; </script>';
			} 
			else {
				if (move_uploaded_file($_FILES["rfile"]["tmp_name"], $target_file)) {
					$imagename=basename( $_FILES["rfile"]["name"]);	
					
					
					$sql2 = "INSERT INTO ngo_profile VALUES ('$user_id', '$ngoname', '$address', '$city', '$district', '$state', '$pincode', 
															'$mobileno', '$phoneno', '$email', '$website', '$contactperson', '$sec', '$aim', '$imagename')";
					if($conn->query($sql2) == true)
					{
					
						$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?id=" . $user_id;
						$toEmail = $email;
						$subject = "User Registration Activation Email";
						$content = "Click this link to activate your account " 
												. $actual_link ;
						$mailHeaders = "From: Admin\r\n";
						if(mail($toEmail, $subject, $content, $mailHeaders)) {
							$_SESSION['message'] = array("Email with verification link has been sent"); 
							header("location:loginhome.php");
						}else{
							$_SESSION['error'] = array("Invalid Email ID");
							header("location:loginhome.php");
						}
					}
					
				} else {
					echo '<script> alert("Error uploading file"); history.go(-1) </script>';
				}
			}
			
		}
		else{
			$_SESSION['error'] = array("Could not complete registration");
			header("location:loginhome.php");
		}
	}	
		unset($_POST);
}

if(!empty($_GET["id"]))
	{
		$userid = $_GET["id"];
		
		$sql1 = "SELECT * FROM user_account WHERE user_profile_id = '$userid'";
		$result = mysqli_query($conn, $sql1);
		
		if(mysqli_num_rows($result) > 0)
		{
			while($row = mysqli_fetch_assoc($result)){
				$status = $row["user_status"];
			}
			if($status == 'ACTIVE' || $status == 'PENDING')
			{
				echo '<script> alert("Account already verified") </script>';
				echo '<script> window.location="loginhome.php"; </script>';
			}
			else
			{
				$sql2 = "UPDATE user_account SET user_status='PENDING' WHERE user_profile_id = '$userid'";
				if($conn->query($sql2)=== true)
				{
					echo '<script> alert("Account sent to Admin for verification") </script>';
					echo '<script> window.location="loginhome.php"; </script>';
				}
				else
				{
					echo '<script> alert("Problem in account activation") </script>';
				}
			}
		}
	}



?>