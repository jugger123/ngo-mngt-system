<?PHP
		session_start();
		include("config.php");
		include("header.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>NGO Details Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">Compare NGOs
            </h3>
            <hr>
			 <form action="comparengos.php" method="post">
			 <div id="custom-search-input" style="background-color:#f6f6f6">
			<div class="input-group col-md-12">
				<div class="row">
					<?php 
				$conn = mysqli_connect($host,$username,$password,$db_name);
				$page=null;
				
				$q1="Select name_ngo from ngo_profile"; 
								
				$result = $conn->query($q1);

				if ($result->num_rows > 0) {
					// output data of each row
					?><select name="s1" style="margin-top:1%"><?php
					while($row = $result->fetch_assoc()) {
				
				?>
				
				  <option value="<?php echo $row['name_ngo']; ?>"><?php echo $row['name_ngo']; ?></option>
				
				<?php
					}
				}
				?></select>
					</div>
				<div class="row">
				
				<?php
				$q1="Select name_ngo from ngo_profile"; 
								
				$result = $conn->query($q1);

				if ($result->num_rows > 0) {
					// output data of each row
					?><select name="s2" style="margin-top:5%; margin-bottom:1%;"><?php
					while($row = $result->fetch_assoc()) {
				
				?>
				
				  <option value="<?php echo $row['name_ngo']; ?>"><?php echo $row['name_ngo']; ?></option>
				
				<?php
					}
				}
				?></select>
				</div>
                    <!--<input type="text" class="form-control input-lg" placeholder="Enter NGO 1 Name" name="searchele1"/><br/>
                    <input type="text" class="form-control input-lg" placeholder="Enter NGO 2 Name" name="searchele2" />-->
                    <span class="input-group-btn" style="margin-left:20%">
                        <button class="btn btn-info btn-lg" type="submit" name="compare" id="compare">
                            Compare
                        </button>
                    </span>
                </div>
            </div>
			</div>
			</form>
		  </div>
		</div>
	</div>
</header>

<?php
$conn = mysqli_connect($host,$username,$password,$db_name);
	$page=null;
	
  //   $_SESSION['search_text']= $ele;
	 if(isset($_POST['compare']))
		   {
?>
<section class="bg-primary" id="profile">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">Profile</h2>
            <hr class="light my-4">
			
<?php

	
		  
		  $ngo1=$_POST['s1'];
		  $ngo2=$_POST['s2'];
		  
		  if($ngo1 == NULL or $ngo2 == NULL){
			 echo "<h3>** Select NGOs to Compare **</h3>";
		  }
		  else{
				
							$sql1 = "SELECT * FROM ngo_profile WHERE name_ngo = '$ngo1'";
							$sql2 = "SELECT * FROM ngo_profile WHERE name_ngo = '$ngo2'";
							$result1 = mysqli_query($conn, $sql1);
							$result2 = mysqli_query($conn, $sql2);
							
							if(mysqli_num_rows($result1) > 0 || mysqli_num_rows($result2) > 0){
								$row1 = mysqli_fetch_assoc($result1);
								$row2 = mysqli_fetch_assoc($result2);
								
								$uid1=$row1["user_profile_id"];
								$uid2=$row2["user_profile_id"];
					?>
								
							<table class="table">	
								<tbody>
									<tr>
									<th>NGO Name</th>
									<td><?php echo $row1["name_ngo"]; ?></td>
									<td><?php echo $row2["name_ngo"]; ?></td>
									</tr>
									<tr>
									<th>Address</th>
									<td><?php echo $row1["address"]; ?></td>
									<td><?php echo $row2["address"]; ?></td>
									</tr>
									<tr>
									<th>City</th>
									<td><?php echo $row1["city"]; ?></td>
									<td><?php echo $row2["city"]; ?></td>
									</tr>
									<tr>
									<th>District</th>
									<td><?php echo $row1["district"]; ?></td>
									<td><?php echo $row2["district"]; ?></td>
									</tr>
									<tr>
									<th>State</th>
									<td><?php echo $row1["state"]; ?></td>
									<td><?php echo $row2["state"]; ?></td>
									</tr>
									<tr>
									<th>Pincode</th>
									<td><?php echo $row1["pincode"]; ?></td>
									<td><?php echo $row2["pincode"]; ?></td>
									</tr>
									<tr>
									<th>Mobile Number</th>
									<td><?php echo $row1["mobileno"]; ?></td>
									<td><?php echo $row2["mobileno"]; ?></td>
									</tr>
									<tr>
									<th>Phone Number</th>
									<td><?php echo $row1["phoneno"]; ?></td>
									<td><?php echo $row2["phoneno"]; ?></td>
									</tr>
									<tr>
									<th>Email</th>
									<td><?php echo $row1["email"]; ?></td>
									<td><?php echo $row2["email"]; ?></td>
									</tr>
									<tr>
									<th>Website</th>
									<td><?php echo $row1["website"]; ?></td>
									<td><?php echo $row2["website"]; ?></td>
									</tr>
									<tr>
									<th>Contact Person</th>
									<td><?php echo $row1["contactperson"]; ?></td>
									<td><?php echo $row2["contactperson"]; ?></td>
									</tr>
									<tr>
									<th>Sector</th>
									<td><?php echo $row1["sector"]; ?></td>
									<td><?php echo $row2["sector"]; ?></td>
									</tr>
									<tr>
									<th>Aim/Objective</th>
									<td><?php echo $row1["aim"]; ?></td>
									<td><?php echo $row2["aim"]; ?></td>
									</tr>
								</tbody>
						</table>	
						<?php
								
							}
							else
							{
						?>
							<h3>** Details not present at the moment **</h3>
							
					<?php
							}
						  
					?>
			
          </div>
        </div>
      </div>
    </section>
	
	<section>
		<div class="container">
			<div class="row">
			  <div class="col-lg-12 text-center">
				<h2 class="section-heading">Financial Details</h2>
				<hr class="my-4">
			  </div>
			</div>
		  </div>
		  <div class="container">
			<div class="row">
			  <div class="col-lg-12 mx-auto text-center">
			  <div class="w3-container">
					<div class="w3-row">
						<a href="javascript:void(0)" onclick="openCity(event, 'Income');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red">Income</div>
						</a>
						<a href="javascript:void(0)" onclick="openCity(event, 'Expenses');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Expenses</div>
						</a>
						<a href="javascript:void(0)" onclick="openCity(event, 'Assets');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Assets</div>
						</a>
						<a href="javascript:void(0)" onclick="openCity(event, 'Liabilities');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Liabilities</div>
						</a>
					 </div>

					  <div id="Income" class="w3-container city">
					
					<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid1'";
							$sql2 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid2'";
							$result1 = mysqli_query($conn, $sql1);
							$result2 = mysqli_query($conn, $sql2);
							
							if(mysqli_num_rows($result1) > 0 || mysqli_num_rows($result2) > 0){
								$row1 = mysqli_fetch_assoc($result1);
								$row2 = mysqli_fetch_assoc($result2);
								
					?>
							<table class="table">
								<tbody>
								<tr>
									<th>NGO Name</th>
									<td><?php echo $ngo1; ?></td>
									<td><?php echo $ngo2; ?></td>
									</tr>
									<tr>
									<th>Government Sources</th>
									<td><?php echo $row1["govt_sources"]; ?></td>
									<td><?php echo $row2["govt_sources"]; ?></td>
									</tr>
									<tr>
									<th>Institutional Sources</th>
									<td><?php echo $row1["inst_sources"]; ?></td>
									<td><?php echo $row2["inst_sources"]; ?></td>
									</tr>
									<tr>
									<th>Foreign Sources</th>
									<td><?php echo $row1["foreign_sources"]; ?></td>
									<td><?php echo $row2["foreign_sources"]; ?></td>
									</tr>
									<tr>
									<th>Donations</th>
									<td><?php echo $row1["donations"]; ?></td>
									<td><?php echo $row2["donations"]; ?></td>
									</tr>
									<tr>
									<th>Sales/Fees</th>
									<td><?php echo $row1["sales"]; ?></td>
									<td><?php echo $row2["sales"]; ?></td>
									</tr>
									<tr>
									<th>Interest/Dividend</th>
									<td><?php echo $row1["interest"]; ?></td>
									<td><?php echo $row2["interest"]; ?></td>
									</tr>
									<tr>
									<th>Other Income</th>
									<td><?php echo $row1["others_income"]; ?></td>
									<td><?php echo $row2["others_income"]; ?></td>
									</tr>
									<tr>
									<th>Total Income</th>
									<td><?php echo $row1["total_income"]; ?></td>
									<td><?php echo $row2["total_income"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?>
					
							
					  </div>

					  <div id="Expenses" class="w3-container city" style="display:none">
						<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid1'";
							$sql2 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid2'";
							$result1 = mysqli_query($conn, $sql1);
							$result2 = mysqli_query($conn, $sql2);
							
							if(mysqli_num_rows($result1) > 0 || mysqli_num_rows($result2) > 0){
								$row1 = mysqli_fetch_assoc($result1);
								$row2 = mysqli_fetch_assoc($result2);
					?>
							<table class="table">
								<tbody><tr>
									<th>NGO Name</th>
									<td><?php echo $ngo1; ?></td>
									<td><?php echo $ngo2; ?></td>
									</tr>
									<tr>
									<th>Direct Programme Activities</th>
									<td><?php echo $row1["activities"]; ?></td>
									<td><?php echo $row2["activities"]; ?></td>
									</tr>
									<tr>
									<th>Staff Cost</th>
									<td><?php echo $row1["staff_cost"]; ?></td>
									<td><?php echo $row2["staff_cost"]; ?></td>
									</tr>
									<tr>
									<th>Travel Expenses</th>
									<td><?php echo $row1["travel"]; ?></td>
									<td><?php echo $row2["travel"]; ?></td>
									</tr>
									<tr>
									<th>Funds Raising Expenses</th>
									<td><?php echo $row1["funds_raised"]; ?></td>
									<td><?php echo $row2["funds_raised"]; ?></td>
									</tr>
									<tr>
									<th>Overhead Expenses</th>
									<td><?php echo $row1["overhead"]; ?></td>
									<td><?php echo $row2["overhead"]; ?></td>
									</tr>
									<tr>
									<th>Depreciation</th>
									<td><?php echo $row1["depreciation"]; ?></td>
									<td><?php echo $row2["depreciation"]; ?></td>
									</tr>
									<tr>
									<th>Other Expenses</th>
									<td><?php echo $row1["others_expenses"]; ?></td>
									<td><?php echo $row2["others_expenses"]; ?></td>
									</tr>
									<tr>
									<th>Total Expenses</th>
									<td><?php echo $row1["total_expenses"]; ?></td>
									<td><?php echo $row2["total_expenses"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?> 
					  </div>

					  <div id="Assets" class="w3-container city" style="display:none">
						<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid1'";
							$sql2 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid2'";
							$result1 = mysqli_query($conn, $sql1);
							$result2 = mysqli_query($conn, $sql2);
							
							if(mysqli_num_rows($result1) > 0 || mysqli_num_rows($result2) > 0){
								$row1 = mysqli_fetch_assoc($result1);
								$row2 = mysqli_fetch_assoc($result2);
					?>
							<table class="table">
								<tbody><tr>
									<th>NGO Name</th>
									<td><?php echo $ngo1; ?></td>
									<td><?php echo $ngo2; ?></td>
									</tr>
									<tr>
									<th>Fixed Assets</th>
									<td><?php echo $row1["fixed_assets"]; ?></td>
									<td><?php echo $row2["fixed_assets"]; ?></td>
									</tr>
									<tr>
									<th>Current Assets</th>
									<td><?php echo $row1["current_assets"]; ?></td>
									<td><?php echo $row2["current_assets"]; ?></td>
									</tr>
									<tr>
									<th>Cash/Bank</th>
									<td><?php echo $row1["cash_bank"]; ?></td>
									<td><?php echo $row2["cash_bank"]; ?></td>
									</tr>
									<tr>
									<th>Investments</th>
									<td><?php echo $row1["investments"]; ?></td>
									<td><?php echo $row2["investments"]; ?></td>
									</tr>
									<tr>
									<th>Accumulated Deficits</th>
									<td><?php echo $row1["accumulated_deficits"]; ?></td>
									<td><?php echo $row2["accumulated_deficits"]; ?></td>
									</tr>
									<tr>
									<th>Total Assets</th>
									<td><?php echo $row1["total_assets"]; ?></td>
									<td><?php echo $row2["total_assets"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?> 
					  </div>
					  <div id="Liabilities" class="w3-container city" style="display:none">
						<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid1'";
							$sql2 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$uid2'";
							$result1 = mysqli_query($conn, $sql1);
							$result2 = mysqli_query($conn, $sql2);
							
							if(mysqli_num_rows($result1) > 0 || mysqli_num_rows($result2) > 0){
								$row1 = mysqli_fetch_assoc($result1);
								$row2 = mysqli_fetch_assoc($result2);
					?>
							<table class="table">
								<tbody><tr>
									<th>NGO Name</th>
									<td><?php echo $ngo1; ?></td>
									<td><?php echo $ngo2; ?></td>
									</tr>
									<tr>
									<th>Share Capital</th>
									<td><?php echo $row1["capital"]; ?></td>
									<td><?php echo $row2["capital"]; ?></td>
									</tr>
									<tr>
									<th>General Funds</th>
									<td><?php echo $row1["general_funds"]; ?></td>
									<td><?php echo $row2["general_funds"]; ?></td>
									</tr>
									<tr>
									<th>Earmarked Funds</th>
									<td><?php echo $row1["earmarked_funds"]; ?></td>
									<td><?php echo $row2["earmarked_funds"]; ?></td>
									</tr>
									<tr>
									<th>Loans</th>
									<td><?php echo $row1["loans"]; ?></td>
									<td><?php echo $row2["loans"]; ?></td>
									</tr>
									<tr>
									<th>Current Liabilities</th>
									<td><?php echo $row1["current_liabilities"]; ?></td>
									<td><?php echo $row2["current_liabilities"]; ?></td>
									</tr>
									<tr>
									<th>Total Liabilities</th>
									<td><?php echo $row1["total_liabilities"]; ?></td>
									<td><?php echo $row2["total_liabilities"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
		  }
						?> 
					  </div>
				</div>	  
				</div>
			</div>
		</div>
    </section>
	
	
		   <?php } ?>
<script>
	function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}
</script>
	
	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>