<?PHP
		session_start();
		include("config.php");
		include("header.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>NGO Details Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">
              <strong><?php echo $_GET["name"]; ?> </strong>
            </h3>
            <hr>
		  </div>
		  <div class="col-lg-8 mx-auto">
		  
			<a href="volunteer.php?id=<?php echo $_GET["id"]; ?>" class="btn btn-primary" role="button">Volunteer</a>
			<a href="donate.php?id=<?php echo $_GET["id"]; ?>&name=<?php echo $_GET["name"]; ?>" class="btn btn-primary" role="button" style="margin-left:20px">Donate</a>
		  
		  </div>
		</div>
	</div>
</header>


<section class="bg-primary" id="profile">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white">Profile</h2>
            <hr class="light my-4">
			
<?php

	$conn = mysqli_connect($host,$username,$password,$db_name);
	$page=null;
	if(isset($_GET["id"])) {
		$ele=$_GET["id"];
	}
     $_SESSION['search_text']= $ele;
	 if($ele == null)
		   {
		   		echo "<script> alert('please enter text')
				history.go(-1);</script>";
				//header('Location:home.php');
		   }
	else{
?>

					
					<?php 
						
							$sql = "SELECT * FROM ngo_profile WHERE user_profile_id = '$ele'";
							$result = mysqli_query($conn, $sql);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
							<div class="table-responsive">
							<table class="table">	
								<tbody>
									<tr>
									<th>Address</th>
									<td><?php echo $row["address"]; ?></td>
									</tr>
									<tr>
									<th>City</th>
									<td><?php echo $row["city"]; ?></td>
									</tr>
									<tr>
									<th>District</th>
									<td><?php echo $row["district"]; ?></td>
									</tr>
									<tr>
									<th>State</th>
									<td><?php echo $row["state"]; ?></td>
									</tr>
									<tr>
									<th>Pincode</th>
									<td><?php echo $row["pincode"]; ?></td>
									</tr>
									<tr>
									<th>Mobile Number</th>
									<td><?php echo $row["mobileno"]; ?></td>
									</tr>
									<tr>
									<th>Phone Number</th>
									<td><?php echo $row["phoneno"]; ?></td>
									</tr>
									<tr>
									<th>Email</th>
									<td><?php echo $row["email"]; ?></td>
									</tr>
									<tr>
									<th>Website</th>
									<td><?php echo $row["website"]; ?></td>
									</tr>
									<tr>
									<th>Contact Person</th>
									<td><?php echo $row["contactperson"]; ?></td>
									</tr>
									<tr>
									<th>Sector</th>
									<td><?php echo $row["sector"]; ?></td>
									</tr>
									<tr>
									<th>Aim/Objective</th>
									<td><?php echo $row["aim"]; ?></td>
									</tr>
								</tbody>
						</table>	
						</div>
						<?php
								
							}
							else
							{
						?>
							<h3>** Financials not present at the moment **</h3>
							
					<?php
							}
						}  
					?>
			
          </div>
        </div>
      </div>
    </section>
	
	<section>
		<div class="container">
			<div class="row">
			  <div class="col-lg-12 text-center">
				<h2 class="section-heading">Financial Details</h2>
				<hr class="my-4">
			  </div>
			</div>
		  </div>
		  <div class="container">
			<div class="row">
			  <div class="col-lg-12 mx-auto text-center">
			  <div class="col-xs-12 w3-container">
					<div class="col-xs-12 w3-row">
						<a href="javascript:void(0)" onclick="openCity(event, 'Income');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-red col-xs-3">Income</div>
						</a>
						<a href="javascript:void(0)" onclick="openCity(event, 'Expenses');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding col-xs-3">Expenses</div>
						</a>
						<a href="javascript:void(0)" onclick="openCity(event, 'Assets');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding col-xs-3">Assets</div>
						</a>
						<a href="javascript:void(0)" onclick="openCity(event, 'Liabilities');">
						  <div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding col-xs-3">Liabilities</div>
						</a>
					</div>
				</div>

					  <div id="Income" class="w3-container city">
					
					<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$ele'";
							$result = mysqli_query($conn, $sql1);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>  <div class="table-responsive">
							<table class="table">
								<tbody>
									<tr>
									<th>Government Sources</th>
									<td><?php echo $row["govt_sources"]; ?></td>
									</tr>
									<tr>
									<th>Institutional Sources</th>
									<td><?php echo $row["inst_sources"]; ?></td>
									</tr>
									<tr>
									<th>Foreign Sources</th>
									<td><?php echo $row["foreign_sources"]; ?></td>
									</tr>
									<tr>
									<th>Donations</th>
									<td><?php echo $row["donations"]; ?></td>
									</tr>
									<tr>
									<th>Sales/Fees</th>
									<td><?php echo $row["sales"]; ?></td>
									</tr>
									<tr>
									<th>Interest/Dividend</th>
									<td><?php echo $row["interest"]; ?></td>
									</tr>
									<tr>
									<th>Other Income</th>
									<td><?php echo $row["others_income"]; ?></td>
									</tr>
									<tr>
									<th>Total Income</th>
									<td><?php echo $row["total_income"]; ?></td>
									</tr>
								</tbody>
						</table>
						</div>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?>
					
							
					  </div>

					  <div id="Expenses" class="w3-container city" style="display:none">
						<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$ele'";
							$result = mysqli_query($conn, $sql1);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
							<table class="table">
								<tbody>
									<tr>
									<th>Direct Programme Activities</th>
									<td><?php echo $row["activities"]; ?></td>
									</tr>
									<tr>
									<th>Staff Cost</th>
									<td><?php echo $row["staff_cost"]; ?></td>
									</tr>
									<tr>
									<th>Travel Expenses</th>
									<td><?php echo $row["travel"]; ?></td>
									</tr>
									<tr>
									<th>Funds Raising Expenses</th>
									<td><?php echo $row["funds_raised"]; ?></td>
									</tr>
									<tr>
									<th>Overhead Expenses</th>
									<td><?php echo $row["overhead"]; ?></td>
									</tr>
									<tr>
									<th>Depreciation</th>
									<td><?php echo $row["depreciation"]; ?></td>
									</tr>
									<tr>
									<th>Other Expenses</th>
									<td><?php echo $row["others_expenses"]; ?></td>
									</tr>
									<tr>
									<th>Total Expenses</th>
									<td><?php echo $row["total_expenses"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?> 
					  </div>

					  <div id="Assets" class="w3-container city" style="display:none">
						<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$ele'";
							$result = mysqli_query($conn, $sql1);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
							<table class="table">
								<tbody>
									<tr>
									<th>Fixed Assets</th>
									<td><?php echo $row["fixed_assets"]; ?></td>
									</tr>
									<tr>
									<th>Current Assets</th>
									<td><?php echo $row["current_assets"]; ?></td>
									</tr>
									<tr>
									<th>Cash/Bank</th>
									<td><?php echo $row["cash_bank"]; ?></td>
									</tr>
									<tr>
									<th>Investments</th>
									<td><?php echo $row["investments"]; ?></td>
									</tr>
									<tr>
									<th>Accumulated Deficits</th>
									<td><?php echo $row["accumulated_deficits"]; ?></td>
									</tr>
									<tr>
									<th>Total Assets</th>
									<td><?php echo $row["total_assets"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?> 
					  </div>
					  <div id="Liabilities" class="w3-container city" style="display:none">
						<?php 
						
							$sql1 = "SELECT * FROM ngo_financials WHERE user_profile_id = '$ele'";
							$result = mysqli_query($conn, $sql1);
							
							if(mysqli_num_rows($result) > 0){
								$row = mysqli_fetch_assoc($result)
					?>
							<table class="table">
								<tbody>
									<tr>
									<th>Share Capital</th>
									<td><?php echo $row["capital"]; ?></td>
									</tr>
									<tr>
									<th>General Funds</th>
									<td><?php echo $row["general_funds"]; ?></td>
									</tr>
									<tr>
									<th>Earmarked Funds</th>
									<td><?php echo $row["earmarked_funds"]; ?></td>
									</tr>
									<tr>
									<th>Loans</th>
									<td><?php echo $row["loans"]; ?></td>
									</tr>
									<tr>
									<th>Current Liabilities</th>
									<td><?php echo $row["current_liabilities"]; ?></td>
									</tr>
									<tr>
									<th>Total Liabilities</th>
									<td><?php echo $row["total_liabilities"]; ?></td>
									</tr>
								</tbody>
						</table>
						<?php
								
							}
							else
							{
						?>
							<br><h4>** Financials not present at the moment **</h4>
							
					<?php
							}
							
						?> 
					  </div>
				</div>	  
				</div>
			</div>
		</div>
    </section>
	
<script>
	function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}
</script>
	
	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>