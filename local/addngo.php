<?php

include("config.php"); 

		$conn = new mysqli($host, $username, $password, $db_name);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 

		if(isset($_POST['submitngo'])){
			
		$ngoname=$_POST['ngoname'];
		$nemail=$_POST['nemail'];
		$aim=$_POST['aim'];
		$address=$_POST['address'];
		$city=$_POST['city'];
		$district=$_POST['district'];
		$state=$_POST['state'];
		$pincode=$_POST['pincode'];
		$mbno=$_POST['mbno'];
		$phoneno=$_POST['phoneno'];
		$website=$_POST['website'];
		$contactp=$_POST['contactp'];
		
		$sectorv="";
		
		if(!empty($_POST['sector'])){
		// Loop to store and display values of individual checked checkbox.
			foreach($_POST['sector'] as $selected){
				$sectorv=$sectorv." ".$selected;
			}
		}
		
		$today = getdate();
		$year = $today['year'];

		$salt1 = substr(hash('sha256', mt_rand() . microtime()), 0, 10); 
		$user_id = $year . $salt1;
		
		$salt2 = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$saltedpasswd = $salt2 . $password;
		$hashpassword = hash('sha256', $saltedpasswd);
		
		$sql="insert into ngo_profile (user_profile_id,name_ngo,address,city,district,state,pincode,mobileno,
			phoneno,email,website,contactperson,sector,aim,registration_certificate) values ('$user_id','$ngoname','$address',
			'$city','$district','$state','$pincode','$mbno','$phoneno','$nemail','$website',
			'$contactp','$sectorv','$aim','')";
			
		$sql1 = "INSERT INTO user_account VALUES ('$user_id', '$nemail', '$hashpassword', '$salt2', 'sha256', 'ngo', 'EMAIL VERIFICATION', 'N')";
			
		$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream',
	 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel',
	  'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['fdfile']['name']) && in_array($_FILES['fdfile']['type'],$file_mimes)){
        if(is_uploaded_file($_FILES['fdfile']['tmp_name'])){
            $csv_file = fopen($_FILES['fdfile']['tmp_name'], 'r');
            
			while(($myData = fgetcsv($csv_file)) !== FALSE){
				$sql2 = "insert into ngo_financials (user_profile_id,govt_sources,inst_sources,foreign_sources,donations,sales,
						interest,others_income,total_income,activities,staff_cost,travel,funds_raised,overhead,
						depreciation,others_expenses,total_expenses,fixed_assets,current_assets,cash_bank,investments,
						accumulated_deficits,total_assets,capital,general_funds,earmarked_funds,loans,current_liabilities,
						total_liabilities) values ('$user_id','".$myData[0]."','".$myData[1]."','".$myData[2]."','".$myData[3]."',
						'".$myData[4]."','".$myData[5]."','".$myData[6]."','".$myData[7]."','".$myData[8]."',
						'".$myData[9]."','".$myData[10]."','".$myData[11]."','".$myData[12]."','".$myData[13]."'
						,'".$myData[14]."','".$myData[15]."','".$myData[16]."','".$myData[17]."','".$myData[18]."'
						,'".$myData[19]."','".$myData[20]."','".$myData[21]."','".$myData[22]."','".$myData[23]."'
						,'".$myData[24]."','".$myData[25]."','".$myData[26]."','".$myData[27]."')";
			}
		}
	}
	else{
		
		echo "<script> alert('Select financials details file'); </script>";
		echo '<script> window.location="admin.php#add"; </script>';
		
	}
		
		if($conn->query($sql) === TRUE && mysqli_query($conn,$sql2)==TRUE && mysqli_query($conn,$sql1)==TRUE)
		{
				$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?id=" . $user_id;
				$toEmail = $nemail;
				$subject = "NGO Registration Activation Email";
				$content = "Click this link to activate your account " 
								. $actual_link ;
				$mailHeaders = "From: Admin\r\n";
				if(mail($toEmail, $subject, $content, $mailHeaders)) {
					$_SESSION['message'] = array("Successfully added and Email with verification link has been sent"); 
					header("location:admin.php");
				}else{
					$_SESSION['error'] = array("Could not sent email");
					header("location:admin.php");
				}
			
				unset($_POST);
			//echo "<script> alert('Success');
			//history.go(-1);</script>";
			//header("Location: admin.php".$insert_status);
		}
		else{
			echo "<script> alert('Failed to Add');</script>";
		}	
	}
	
	if(!empty($_GET["id"]))
	{
		$userid = $_GET["id"];
		
		$sql1 = "SELECT * FROM user_account WHERE user_profile_id = '$userid'";
		$result = mysqli_query($conn, $sql1);
		
		if(mysqli_num_rows($result) > 0)
		{
			while($row = mysqli_fetch_assoc($result)){
				$status = $row["user_status"];
			}
			if($status == 'ACTIVE')
			{
				echo '<script> alert("Account already activated") </script>';
				echo '<script> window.location="loginhome.php"; </script>';
			}
			else
			{
				$sql2 = "UPDATE user_account SET user_status='ACTIVE' WHERE user_profile_id = '$userid'";
				if($conn->query($sql2)=== true)
				{
					echo '<script> alert("Account Activated") </script>';
					echo '<script> window.location="loginhome.php"; </script>';
				}
				else
				{
					echo '<script> alert("Problem in account activation") </script>';
				}
			}
		}
	}
	
?>