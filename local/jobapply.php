<?php

	session_start();
	include("config.php");
	
	// Connect to server and select databse.
	$conn=mysqli_connect($host,$username,$password,$db_name);
	if($conn->connect_error){
		die("Connection Error: ". $conn->connect_error);
	}
	
	
	if(!isset($_SESSION["user_id"]))
	{
		echo '<script> alert("Please login to continue"); </script>';
		echo '<script> window.location="loginhome.php"; </script>';
		
	}
	else
	{
		if($_SESSION["user_type"] == "ngo" or $_SESSION["user_type"] == "admin")
		{
			echo '<script> alert("Restricted Access"); history.go(-1);</script>';
		}
	}
	

	include("header.php");
	
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Jobs Apply Page</title>

</head>

<body id="page-top">

<header class="masthead text-center text-white d-flex">

      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h3 class="text-uppercase">
              <strong>Job Application </strong>
            </h3>
            <hr>
		  </div>
		  <div class="col-lg-8 mx-auto">
		  
			<form id="userform" class="form-horizontal" role="form" method="POST" action="jobapp.php" enctype="multipart/form-data"> 
				<input type="hidden" id="jobid" name="jobid" value="<?php echo $_GET["jid"]; ?>"/>
				<div class="form-group">
								<label for="file" class="label col-md-3">Resume :</label>
								<!--<form action="ngoform.php" method="post" enctype="multipart/form-data">-->					
								<input class="col-md-6 control-label" type="file"  id="rfile" name="rfile" required>
				</div><br>
				<button id="donate-btn" type="submit" name="submitr" class="btn btn-success"><i class="icon-hand-right"></i> Submit</button>
			</form>
		  </div>
		</div>
	</div>
</header>

<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/uploadfile.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

<?php include("footer.html"); ?>
</body>
</html>