-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.29-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ngo
CREATE DATABASE IF NOT EXISTS `ngo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ngo`;

-- Dumping structure for table ngo.event_participants
CREATE TABLE IF NOT EXISTS `event_participants` (
  `user_id` varchar(50) DEFAULT NULL,
  `event_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.event_participants: ~4 rows (approximately)
/*!40000 ALTER TABLE `event_participants` DISABLE KEYS */;
INSERT INTO `event_participants` (`user_id`, `event_id`) VALUES
	('201829eee40c24', '2018ab743918cc'),
	('201829eee40c24', '2018573e5770d6'),
	('201829eee40c24', '2018b4aa98b87b'),
	('201829eee40c24', '2018ab743918cc');
/*!40000 ALTER TABLE `event_participants` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_data
CREATE TABLE IF NOT EXISTS `ngo_data` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `year_estd` varchar(50) DEFAULT NULL,
  `financial_score` int(11) DEFAULT NULL,
  `description` longtext,
  `mission` longtext,
  `vision` longtext,
  `trustee1` varchar(50) DEFAULT NULL,
  `trustee2` varchar(50) DEFAULT NULL,
  `trustee3` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `ngo_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ngo_data` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_donor
CREATE TABLE IF NOT EXISTS `ngo_donor` (
  `user_id` varchar(50) DEFAULT NULL,
  `ngo_id` varchar(50) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_donor: ~5 rows (approximately)
/*!40000 ALTER TABLE `ngo_donor` DISABLE KEYS */;
INSERT INTO `ngo_donor` (`user_id`, `ngo_id`, `amount`, `transaction_id`) VALUES
	('201829eee40c24', '2018b905e51aab', 1000, '2018c56629604393700a5674'),
	('201829eee40c24', '2018b905e51aab', 1000, '20185350d92c67c58f4b05e5'),
	('201829eee40c24', '2018b905e51aab', 1000, '2018b9dfc08c06a5a94aefdf'),
	('201829eee40c24', '2018b905e51aab', 2000, '20184a668ae84c4dc7b1543a'),
	('201829eee40c24', '2018b905e51aab', 1000, '2018fcc5d9a61a53897ade45');
/*!40000 ALTER TABLE `ngo_donor` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_events
CREATE TABLE IF NOT EXISTS `ngo_events` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `event_id` varchar(50) DEFAULT NULL,
  `event_name` varchar(50) DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `event_place` varchar(50) DEFAULT NULL,
  `about` longtext,
  `image` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_events: ~4 rows (approximately)
/*!40000 ALTER TABLE `ngo_events` DISABLE KEYS */;
INSERT INTO `ngo_events` (`user_profile_id`, `event_id`, `event_name`, `event_date`, `event_time`, `event_place`, `about`, `image`) VALUES
	('2018b905e51aab', '2018ab743918cc', 'Plant Trees', '2018-02-03', '10:10:00', 'Mehdipatnam, Hyderabad', 'Planting trees initiative', ''),
	('2018b905e51aab', '2018573e5770d6', 'Annual Event', '2018-02-05', '22:10:00', 'Mehdipatnam, Hyderabad', 'Annual day at school', ''),
	('2018b905e51aab', '2018b4aa98b87b', 'Donation', '2018-02-14', '09:02:00', 'Mumbai', 'Raise funds', '');
/*!40000 ALTER TABLE `ngo_events` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_financials
CREATE TABLE IF NOT EXISTS `ngo_financials` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `govt_sources` varchar(50) DEFAULT NULL,
  `inst_sources` varchar(50) DEFAULT NULL,
  `foreign_sources` varchar(50) DEFAULT NULL,
  `donations` varchar(50) DEFAULT NULL,
  `sales` varchar(50) DEFAULT NULL,
  `interest` varchar(50) DEFAULT NULL,
  `others_income` varchar(50) DEFAULT NULL,
  `total_income` varchar(50) DEFAULT NULL,
  `activities` varchar(50) DEFAULT NULL,
  `staff_cost` varchar(50) DEFAULT NULL,
  `travel` varchar(50) DEFAULT NULL,
  `funds_raised` varchar(50) DEFAULT NULL,
  `overhead` varchar(50) DEFAULT NULL,
  `depreciation` varchar(50) DEFAULT NULL,
  `others_expenses` varchar(50) DEFAULT NULL,
  `total_expenses` varchar(50) DEFAULT NULL,
  `fixed_assets` varchar(50) DEFAULT NULL,
  `current_assets` varchar(50) DEFAULT NULL,
  `cash_bank` varchar(50) DEFAULT NULL,
  `investments` varchar(50) DEFAULT NULL,
  `accumulated_deficits` varchar(50) DEFAULT NULL,
  `total_assets` varchar(50) DEFAULT NULL,
  `capital` varchar(50) DEFAULT NULL,
  `general_funds` varchar(50) DEFAULT NULL,
  `earmarked_funds` varchar(50) DEFAULT NULL,
  `loans` varchar(50) DEFAULT NULL,
  `current_liabilities` varchar(50) DEFAULT NULL,
  `total_liabilities` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_financials: ~1 rows (approximately)
/*!40000 ALTER TABLE `ngo_financials` DISABLE KEYS */;
INSERT INTO `ngo_financials` (`user_profile_id`, `govt_sources`, `inst_sources`, `foreign_sources`, `donations`, `sales`, `interest`, `others_income`, `total_income`, `activities`, `staff_cost`, `travel`, `funds_raised`, `overhead`, `depreciation`, `others_expenses`, `total_expenses`, `fixed_assets`, `current_assets`, `cash_bank`, `investments`, `accumulated_deficits`, `total_assets`, `capital`, `general_funds`, `earmarked_funds`, `loans`, `current_liabilities`, `total_liabilities`) VALUES
	('2018b905e51aab', '0', '0', '0', '150,000', '0', '37,293', '55,507', '242,800', '1,258,280', '0', '0', '0', '0', '0', '0', '1,258,280', '0', '36,419', '103,030', '446,608', '6,948,943', '7,535,000', '7,535,000', '0', '0', '0', '0', '7,535,000');
/*!40000 ALTER TABLE `ngo_financials` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_jobs
CREATE TABLE IF NOT EXISTS `ngo_jobs` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `job_id` varchar(50) DEFAULT NULL,
  `job_position` longtext,
  `job_description` longtext,
  `duration` varchar(50) DEFAULT NULL,
  `job_location` varchar(50) DEFAULT NULL,
  `job_type` varchar(50) DEFAULT NULL,
  `job_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `ngo_jobs` DISABLE KEYS */;
INSERT INTO `ngo_jobs` (`user_profile_id`, `job_id`, `job_position`, `job_description`, `duration`, `job_location`, `job_type`, `job_status`) VALUES
	('2018b905e51aab', '2018ed18fe8c60', 'Assistant', 'Assist in conducting events', '3 months', 'Hyderabad', 'Full Time', 'ACTIVE');
/*!40000 ALTER TABLE `ngo_jobs` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_jobs_apply
CREATE TABLE IF NOT EXISTS `ngo_jobs_apply` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `job_id` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `resume` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_jobs_apply: ~0 rows (approximately)
/*!40000 ALTER TABLE `ngo_jobs_apply` DISABLE KEYS */;
INSERT INTO `ngo_jobs_apply` (`user_profile_id`, `job_id`, `date`, `resume`) VALUES
	('201829eee40c24', '2018ed18fe8c60', '2018-02-06', 'Areebuddin.pdf');
/*!40000 ALTER TABLE `ngo_jobs_apply` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_newsletter
CREATE TABLE IF NOT EXISTS `ngo_newsletter` (
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_newsletter: ~2 rows (approximately)
/*!40000 ALTER TABLE `ngo_newsletter` DISABLE KEYS */;
INSERT INTO `ngo_newsletter` (`email`) VALUES
	('areeb.uddin@chidhagni.com'),
	('areeb@gmail.com');
/*!40000 ALTER TABLE `ngo_newsletter` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_profile
CREATE TABLE IF NOT EXISTS `ngo_profile` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `name_ngo` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT 'NIL',
  `state` varchar(50) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `mobileno` bigint(20) DEFAULT NULL,
  `phoneno` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `contactperson` varchar(50) DEFAULT NULL,
  `sector` varchar(100) DEFAULT NULL,
  `aim` varchar(100) DEFAULT NULL,
  `registration_certificate` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_profile: ~1 rows (approximately)
/*!40000 ALTER TABLE `ngo_profile` DISABLE KEYS */;
INSERT INTO `ngo_profile` (`user_profile_id`, `name_ngo`, `address`, `city`, `district`, `state`, `pincode`, `mobileno`, `phoneno`, `email`, `website`, `contactperson`, `sector`, `aim`, `registration_certificate`) VALUES
	('2018b905e51aab', 'H M Charitable Trust', '42, Navketan, Industrial Estate', 'Mumbai', '', 'Maharashtra', 500093, 66756001, 0, '   areeb.uddin@chidhagni.com', 'www.hmgroupindia.com', 'Haresh Mehta', 'Education', 'Support deserving children', ''),
	('20189eb7083a35', 'Kripa Foundation', 'Chapel road Mt. Carmel Church Bandra', 'Mumbai', 'NIL', 'Maharashtra', 400050, 22264054, 22264330, 'kripabandra@gmail.com', 'www.kripafoundation.org', 'Krishna', 'Health', 'Promote Health Safety ', '');
/*!40000 ALTER TABLE `ngo_profile` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_transaction
CREATE TABLE IF NOT EXISTS `ngo_transaction` (
  `transaction_id` varchar(50) DEFAULT NULL,
  `payumoney_id` int(11) DEFAULT NULL,
  `transaction_mode` varchar(50) DEFAULT NULL,
  `transaction_status` varchar(50) DEFAULT NULL,
  `amount_paid` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_transaction: ~4 rows (approximately)
/*!40000 ALTER TABLE `ngo_transaction` DISABLE KEYS */;
INSERT INTO `ngo_transaction` (`transaction_id`, `payumoney_id`, `transaction_mode`, `transaction_status`, `amount_paid`, `timestamp`) VALUES
	('20185350d92c67c58f4b05e5', 0, '', 'failure', '1000.0', '0000-00-00 00:00:00'),
	('20185350d92c67c58f4b05e5', 0, '', 'failure', '1000.0', '0000-00-00 00:00:00'),
	('2018b9dfc08c06a5a94aefdf', 29071, 'CC', 'success', '1000.0', '0000-00-00 00:00:00'),
	('20184a668ae84c4dc7b1543a', 29080, 'CC', 'success', '2000.0', '0000-00-00 00:00:00'),
	('2018fcc5d9a61a53897ade45', 0, '', 'failure', '1000.0', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ngo_transaction` ENABLE KEYS */;

-- Dumping structure for table ngo.ngo_volunteer
CREATE TABLE IF NOT EXISTS `ngo_volunteer` (
  `user_id` varchar(50) DEFAULT NULL,
  `ngo_id` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.ngo_volunteer: ~4 rows (approximately)
/*!40000 ALTER TABLE `ngo_volunteer` DISABLE KEYS */;
INSERT INTO `ngo_volunteer` (`user_id`, `ngo_id`, `status`) VALUES
	('201829eee40c24', '2018b905e51aab', 'INACTIVE'),
	('2018b905e51aab', '2018b905e51aab', 'PENDING'),
	('2018b905e51aab', '2018b905e51aab', 'PENDING'),
	('201829eee40c24', '2018b905e51aab', 'INACTIVE');
/*!40000 ALTER TABLE `ngo_volunteer` ENABLE KEYS */;

-- Dumping structure for table ngo.user_account
CREATE TABLE IF NOT EXISTS `user_account` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `pswd` varchar(256) DEFAULT NULL,
  `pswd_salt` varchar(50) DEFAULT NULL,
  `pswd_hash_fun` varchar(50) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `user_status` varchar(50) DEFAULT NULL,
  `newsletter` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.user_account: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` (`user_profile_id`, `email`, `pswd`, `pswd_salt`, `pswd_hash_fun`, `user_type`, `user_status`, `newsletter`) VALUES
	('20188f2fa30f39', 'areeb@gmail.com', '120c08ecb4b5eb8661297b4b6e4a88505ff990d60bac7fe12ba63e24163b1274', '24b256ccf3dcd3a511d7', 'sha256', 'admin', 'ACTIVE', 'N'),
	('2018b905e51aab', 'areeb.uddin@chidhagni.com', '91694a934eb44ad2e42a7aa531b5ccb8f2e720b22575ca092e19fc26ee39675c', '8dcd1835c2b0749a0a3c', 'sha256', 'ngo', 'INACTIVE', 'N'),
	('201829eee40c24', 'areebuddin95@gmail.com', '322965a1e7011301bbece7a75137bd526cc8a24daf9417a101008a4e8b32055b', '1094c18e12ed34f8106f', 'sha256', 'user', 'ACTIVE', 'N');
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;

-- Dumping structure for table ngo.user_account_status
CREATE TABLE IF NOT EXISTS `user_account_status` (
  `account_status_id` int(11) DEFAULT NULL,
  `account_status_code` varchar(50) DEFAULT NULL,
  `account_status_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.user_account_status: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_account_status` DISABLE KEYS */;
INSERT INTO `user_account_status` (`account_status_id`, `account_status_code`, `account_status_name`) VALUES
	(1, 'ACTIVE', 'Activated'),
	(2, ' EMAIL VERIFICATION', 'Email Verification'),
	(3, 'ADMIN ', 'Admin Approval'),
	(4, 'INACTIVE', 'Deleted');
/*!40000 ALTER TABLE `user_account_status` ENABLE KEYS */;

-- Dumping structure for table ngo.user_profile
CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_profile_id` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ngo.user_profile: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` (`user_profile_id`, `first_name`, `middle_name`, `last_name`, `email`, `country_code`, `phone`) VALUES
	('20188f2fa30f39', 'Areeb', 'uddin', 'Aatif', 'areeb.uddin@chidhagni.com', 91, 9988776655),
	('201829eee40c24', 'Areeb', 'Uddin', 'Aatif', 'areebuddin95@gmail.com', 91, 9502148597);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
